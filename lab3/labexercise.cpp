#include <iostream>
using namespace std;

void display(float,float);
float find_perim(float,float);
float find_area(float,float);

int main()
{   
	float len, wid;
	float perim, area;

	cout << "enter the length: ";
	cin >> len;
	cout << "enter the width: ";
	cin >> wid;

	perim = find_perim(len, wid);  // call the find_perim function
	area = find_area(len, wid);    // call the find_area function

    display(perim, area);          // call the display function

 return 0;
}

void display(float a, float b)
{
	cout<<"The perimeter of the rectangle is "<<a<<" and the area is "<<b<<endl;
}



float find_perim(float a,float b)
{
	float c;
	c = 2*a+2*b;

	return c;
}



float find_area(float a, float b)
{
	float c;
	c = a*b;
	
	return c;
}
