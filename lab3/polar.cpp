//Keegan MacDonell
//Fundamentals of Computing
//09.18.19
//Lab 3: Part 2

#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

//Estabilishes functions
double get_radius(double,double);
double get_angle(double,double);
void quadrant_info(double,double);

int main(){

	double xcoor;
	double ycoor;
	double radius;
	double angle;

	//Obtains user inputs
	cout<<"Please enter the x coordinate of your point: ";
	cin>>xcoor;
	cout<<"Please enter the y coordinate of your point: ";
	cin>>ycoor;

	//Calls functions to compute radius/angle and to display the quadrant
	radius = get_radius(xcoor, ycoor);
	angle = get_angle(xcoor,ycoor);
	cout<<fixed<<setprecision(2);
	cout<<"The radius is "<<radius<<" and the angle is "<<angle<<" degrees. ";
	quadrant_info(xcoor,ycoor);

	return 0;

}

//Computes radius
double get_radius(double x, double y){

	double radius;
	
	radius = sqrt(pow(x,2)+pow(y,2));

	return radius;
}

//Computes angle in degrees
double get_angle(double x, double y){

	double angle;

	angle = atan2(y,x);
	
	if(angle<0)
		angle=((180*angle)/M_PI)+360;
	
	else
		angle = (180*angle)/M_PI;

	return angle;
}

//Uses sign/value of coordinates to find quadrant/axis location
void quadrant_info(double x,double y)
{
	if(x == 0 && y == 0)
		cout<<"The point is on the origin. \n";

	else if(x>0 && y==0)
		cout<<"The point is on the positive x-axis. \n";

	else if(x>0 && y>0)
		cout<<"The point is in Quadrant I. \n";

	else if(x==0 && y>0)
		cout<<"The point is on the positive y-axis. \n";

	else if(x<0 && y>0)
		cout<<"The point is in Quadrant II. \n";

	else if(x<0 && y==0)
		cout<<"The point is on the negative x-axis. \n";

	else if(x<0 && y<0)
		cout<<"The point is in Quadrant III. \n";

	else if(x==0 && y<0)
		cout<<"The point is on the negative y-axis. \n";

	else if(x>0 && y<0)
		cout<<"The point is in Quadrant IV. \n";
	else
		cout<<"Invalid \n";

}
