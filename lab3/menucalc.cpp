//Keegan MacDonell
//Fundamentals of Computing
//09.22.19
//Lab 3: Part 3

#include <iostream>
#include <cmath>
#include <unistd.h>
using namespace std;

//Establishes functions
void menu_display();
void calcdisplay(float,float,int);
float add_func(float,float);
float sub_func(float,float);
float mult_func(float,float);
float div_func(float,float);

int main()
{
	bool run = true;
	int menuchoice;
	float num1;
	float num2;

	
	while(run=true){
		
		//Loop displays options and gets user's operation choice until user enters valid integer
		while(run=true){
			cout<<"What would you like to do? \n";
			menu_display();
			cout<<"Enter your choice: ";
			cin>>menuchoice;

				if(menuchoice>0 && menuchoice<=5)
					break;
				else{
					cout<<menuchoice<< " is not a valid input. Please enter a number 1-5. \n";
					cout<<endl;
				}
		}
		//Breaks out of outer loop when 5 is selected
		if(menuchoice == 5){
				cout<<"Goodbye!"<<endl;
				break;
		}
		
		//Gets user numbers
		cout<<"Enter two numbers: ";
		cin>>num1;
		cin>>num2;
		cout<<"Inputs: "<<num1<<", "<<num2<<endl;

		calcdisplay(num1,num2,menuchoice);		//Performs and displays operation

		usleep(2000000);											//Delays for two seconds so the user can process the answer
	}

return 0;
}



void menu_display(){		//Displays menu choices

	cout<<" 1 for addition \n";
	cout<<" 2 for subtraction \n";
	cout<<" 3 for multiplication \n";
	cout<<" 4 for division \n";
	cout<<" 5 to exit \n";

}


float add_func(float num1, float num2){		//Addition function

	float ans;

	ans = num1+num2;

	return ans;
}


float sub_func(float num1,float num2){		//Subtraction function

	float ans;
	
	ans=num1-num2;

	return ans;
}


float mult_func(float num1, float num2){		//Multiplication function

	float ans;

	ans=num1*num2;

	return ans;
}


float div_func(float num1, float num2){			//Division function

	float ans;

	ans = num1/num2;

	return ans;
}


void calcdisplay(float num1, float num2, int choice){		//Calls functions and displays inputs and outputs

	float ans;
	
	if(choice == 1){
		ans=add_func(num1,num2);
		cout<<"("<<num1<<") + ("<<num2<<") = "<<ans<<endl;
		cout<<endl;
	}

	else if(choice == 2){
		ans=sub_func(num1,num2);
		cout<<"("<<num1<<") - ("<<num2<<") = "<<ans<<endl;
		cout<<endl;
	}

	else if(choice==3){
		ans=mult_func(num1, num2);
		cout<<"("<<num1<<") * ("<<num2<<") = "<<ans<<endl;
		cout<<endl;
	}

	else if(choice ==4){
		ans=div_func(num1,num2);
		cout<<"("<<num1<<") / ("<<num2<<") = "<<ans<<endl;
		cout<<endl;
	}

	else
		cout<<choice<<" is not a valid input. Please enter a number 1-5. \n";
}



