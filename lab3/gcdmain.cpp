//Keegan MacDonell
//Fundamentals of Computing
//09.18.19
//Lab 3: Part 1


#include <iostream>
#include <cmath>
using namespace std;

int getgcd(int,int);

int main()
{

	int firstNum;
	int secNum;
	int gcd;

	cout<<"Please enter the first integer:  ";
	cin>>firstNum;
	cout<<"Please enter the second integer: ";
	cin>>secNum;

	gcd = getgcd(firstNum,secNum); // Calls function

	cout<<"The greatest common denominator of "<<firstNum<<" and "<<secNum<<" is: "<<gcd<<endl;

	return 0;
}

//The function identifies and saves the largest number that can divide both inputs with no remainder
int getgcd(int a, int b)
{
  int saveval;	
  int i;
  int j;

	if(a>=b)				
	{
		for(i=1; i<=b; i++)
		{
			if(a%i == 0 && b%i == 0)
		  	{
			saveval = i;
		  	}
		}
	}
	else if(b>a)
	{
		for(int j=1; j<=a; j++)
		{
			if(a%j == 0 && b%j == 0)
		  	{
			saveval = j;
		  	}
		}	
	}


	return saveval;  //returns gcd
}
