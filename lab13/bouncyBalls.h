//Keegan MacDonell
//Fund Comp
//12.07.19
//Lab 13

class Ball{
	public:
		Ball();
		Ball(int,int,int,float,int,bool,int);
		~Ball();
		int getXCoor();
		int getYCoor();
		double getXVel();
		double getYVel();
		double getStartAngle();
		float getVelocity();
		int getRadius();
		bool getCollisionStatus();
		int getStrength();
		void setXCoor(int);
		void setYCoor(int);
		void setXVel(int);
		void setYVel(int);
		void setStartAngle(int);
		void setVelocity(float);
		void setRadius(int);
		void setCollisionStatus(bool);
		void setStrength(int);
	private:
		int xInfo;
		int yInfo;
		double xVel;
		double yVel;
		double startAngle;
		double velocity;
		int radius;
		bool inCollision;
		int strength;
};


