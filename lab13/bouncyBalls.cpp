//Keegan MacDonell
//Fund Comp
//12.07.19
//Lab 13

#include "bouncyBalls.h"
#include <cmath>

Ball::Ball(){
	xInfo = 10;
	yInfo = 10;
	xVel = 1;
	yVel = 1;
	startAngle = 270;
	velocity = 10;
	radius = 5;
	inCollision=false;
	strength = 3;
}

Ball::Ball(int a, int b, int e, float f, int g, bool h, int i){
	
	xInfo = a;
	yInfo = b;
	startAngle = e;
	velocity = f;
	radius = g;
	xVel = f*cos((e)*M_PI/180);
	yVel = f*sin((e)*M_PI/180);
	inCollision = h;
	strength = i;
}

Ball::~Ball() { }

//Get methods
int Ball::getXCoor(){ return xInfo; }

int Ball::getYCoor(){ return yInfo; }

double Ball::getXVel(){ return xVel; }

double Ball::getYVel(){ return yVel; }

double Ball::getStartAngle() { return startAngle; }

float Ball::getVelocity() { return velocity; }

int Ball::getRadius() { return radius; }

bool Ball::getCollisionStatus() { return inCollision; }

int Ball::getStrength() { return strength; }

//Set methods
void Ball::setXCoor(int a) { xInfo = a; }

void Ball::setYCoor(int a) { yInfo = a; }

void Ball::setXVel(int a) { xVel = a; }

void Ball::setYVel(int a) { yVel = a; }

void Ball::setStartAngle(int a) { startAngle = a; }

void Ball::setVelocity(float a) { velocity = a; }

void Ball::setRadius(int a) { radius = a; }

void Ball::setCollisionStatus(bool a) { inCollision = a; }

void Ball::setStrength(int a) { strength = a; }
