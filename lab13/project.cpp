//Keegan MacDonell
//Fund Comp
//12.07.19
//Lab 13

#include <vector>
#include <cmath>
#include <cstdlib>
#include <unistd.h>
#include <time.h>
#include <bits/stdc++.h>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include "gfx.h"
#include "bouncyBalls.h"

//Prototypes
void player_square(int,int,int&,int, bool&,int&,int&);
void move_Balls(vector<Ball>&, int, int,int&,int&,int&,bool&,long double&);
void display_Balls(vector<Ball>);
void player_Actions(int&,int&, int, int, char, bool&, int, int, vector<Ball>&,bool,long double&);
bool player_Collision_Check(vector<Ball>, int, int,int);
void collision_Confirmation(int&,int&,bool&,vector<Ball>,int,int,int);
void ball_Collision_Check(vector<Ball>&);
void player_Pulse(vector<Ball>&);
void draw_Square(int,int,int);
void fileEnteredBalls(string,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,bool&);
void runLevel(int&,int&,int&,int&,bool&,int&,int&,int,int,vector<Ball>&,bool&,int&, bool&, long double&);
void menuScreen(int&,bool&);
//Sorry for wrap around, not sure how to prevent it short of a vector of vectors and I've gone too far to switch to that now
void game_Over(int&,bool&,int, bool&,int&,int&,int&,int&,int&,int&,int,int,string,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,bool);
void victory_Screen(int&, bool&,int&,int&,int&,int&,int&,int&,int,int,string,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,vector<Ball>&,bool);

int main(int argc, char *argv[]){

	if(argc==1){	//If too few arguments
		cout<<"Please enter a file of asteroids"<<endl;
	}

	if(argc>2){		//If more than one file entered
		cout<<"Too many arguments"<<endl;
	}
	
	if(argc==2){
		string filename = argv[1];			//The filename entered as an argument
		bool errorFlag = false;

		int wd = 1500, ht = 900;			//Initializes board dimensions
		int playerX=wd/2, playerY=ht/2;		//Initializes player starting position
		int playerSize = 27;				//Initializes player size and speed
		int movementSpeed = 35;

		bool run = true;				//Flag to run game loop
		bool playGame = true;

		bool collisionFlag = false;		//Flag to detect ball collisions with player
		int pausetime = 25000;			//Pausetime
		long double startTime;			//Determines startTimes for pulse charging
		bool pulseCharged = false;		//Tells if pulse is charged
	


		vector<Ball> ballVec1;					//Initializes vectors to store balls for different levels
		vector<Ball> ballVec2;
		vector<Ball> ballVec3;
		vector<Ball> ballVec4;
		vector<Ball> ballVec5;
		vector<Ball> ballVec6;
		vector<Ball> ballVec7;
		vector<Ball> ballVec8;
		vector<Ball> ballVec9;
		vector<Ball> ballVec10;
		fileEnteredBalls(filename, ballVec1,ballVec2,ballVec3,ballVec4,ballVec5,ballVec6,ballVec7,ballVec8,ballVec9,ballVec10, errorFlag);		//Loads balls into vectors

		if(errorFlag==true){
			return 1;
		}


		int lifeTracker=0;			//Keeps track of how many times player gets hit
		int lifeCounter=3;			//Tracks player lives
		int level = 0;				//Tracks levels
		int gameOverLevel = 12;		//"Level"  where game over screen is displayed
		int levelsSurvived = 0;		//Keeps track of what level the player makes it to
		char c;						//Gets player inputs from keyboard

		gfx_open(wd,ht, "Don't Touch Me");		//Opens board
		gfx_color(252,252,252);					//Sets color to white

		while(playGame){		//Loop to continually run game

			gfx_clear();
		
			switch(level){							//Switch with cases for different levels
				case 0:
					menuScreen(level,playGame);		//Displays menu
					startTime = time(0);			//Initializes startTime for first level
					break;
				case 1:
					if(run)
						runLevel(playerX, playerY, playerSize, lifeCounter, run, movementSpeed, lifeTracker, wd, ht, ballVec1, collisionFlag, level, pulseCharged, startTime); //Runs level
					else{
						levelsSurvived = level;			//If level is lost, records number
						level=gameOverLevel;			//Displays losing screen
					}
					break;
				case 2:
					if(run)
						runLevel(playerX, playerY, playerSize, lifeCounter, run, movementSpeed, lifeTracker, wd, ht, ballVec2, collisionFlag, level, pulseCharged, startTime);
					else{
						levelsSurvived = level;
						level= gameOverLevel;
					}
					break;
				case 3:
					if(run)
						runLevel(playerX, playerY, playerSize, lifeCounter, run, movementSpeed, lifeTracker, wd, ht, ballVec3, collisionFlag, level, pulseCharged, startTime);
					else{
						levelsSurvived = level;
						level= gameOverLevel;
					}
					break;
				case 4:
					if(run)
						runLevel(playerX, playerY, playerSize, lifeCounter, run, movementSpeed, lifeTracker, wd, ht, ballVec4, collisionFlag, level, pulseCharged, startTime);
					else{
						levelsSurvived = level;
						level = gameOverLevel;
					}
					break;
				case 5:
					if(run)
						runLevel(playerX, playerY, playerSize, lifeCounter, run, movementSpeed, lifeTracker, wd, ht, ballVec5, collisionFlag, level, pulseCharged, startTime);
					else{
						levelsSurvived = level;
						level = gameOverLevel;
					}
					break;
				case 6:
					if(run)
						runLevel(playerX, playerY, playerSize, lifeCounter, run, movementSpeed, lifeTracker, wd, ht, ballVec6, collisionFlag, level, pulseCharged, startTime);
					else{
						levelsSurvived = level;
						level = gameOverLevel;
					}
					break;
				case 7:
					if(run)
						runLevel(playerX, playerY, playerSize, lifeCounter, run, movementSpeed, lifeTracker, wd, ht, ballVec7, collisionFlag, level, pulseCharged, startTime);
					else{
						levelsSurvived = level;
						level = gameOverLevel;
					}
					break;
				case 8:
					if(run)
						runLevel(playerX, playerY, playerSize, lifeCounter, run, movementSpeed, lifeTracker, wd, ht, ballVec8, collisionFlag, level, pulseCharged, startTime);
					else{
						levelsSurvived = level;
						level = gameOverLevel;
					}
					break;
				case 9:
					if(run)
						runLevel(playerX, playerY, playerSize, lifeCounter, run, movementSpeed, lifeTracker, wd, ht, ballVec9, collisionFlag, level, pulseCharged, startTime);
					else{
						levelsSurvived = level;
						level = gameOverLevel;
					}
					break;
				case 10:
					if(run)
						runLevel(playerX, playerY, playerSize, lifeCounter, run, movementSpeed, lifeTracker, wd, ht, ballVec10, collisionFlag, level, pulseCharged, startTime);
					else{
						levelsSurvived = level;
						level = gameOverLevel;
					}
					break;
				case 11:
					victory_Screen(level, playGame, lifeCounter, lifeTracker, playerSize, movementSpeed,playerX,playerY,wd,ht, filename, ballVec1, ballVec2, ballVec3,ballVec4, ballVec5, ballVec6, ballVec7, ballVec8, ballVec9, ballVec10, errorFlag);		//Displays victory screen if user wins
					break;
				case 12:
					game_Over(level,playGame,levelsSurvived, run,lifeCounter,lifeTracker, playerSize, movementSpeed,playerX,playerY,wd,ht,filename,ballVec1,ballVec2,ballVec3,ballVec4,ballVec5,ballVec6,ballVec7,ballVec8,ballVec9,ballVec10,errorFlag);		//Displays Game Over screen
					break;
			}
		
			usleep(pausetime);

		}
	}

	return 0;
}


//Draws player square and tracks lives
void player_square(int xCoor, int yCoor, int &radius, int lifecounter, bool &run, int &movementSpeed, int &lifeTracker){


	switch(lifecounter){
		case 3:
			break;
		case 2:
			if(lifeTracker==1){
				lifeTracker++;		//Increments life counter (so if statement is not entered every time and player size doesnt decrease constantly)
				radius*=0.7;		//Decreases player size and speed
				movementSpeed+=5;
			}
			break;
		case 1:
			if(lifeTracker==3){
				lifeTracker++;
				radius*=0.7;
				movementSpeed+=5;
			}
			break;
		case 0:
			run = false;		//If all lives gone, sets run to false so no more levels are run
			break;
	}

	draw_Square(xCoor, yCoor, radius);
}

//Function to control user inputs on keyboard
void player_Actions(int &playerX, int &playerY, int wd, int ht, char c, bool &run, int playerSize, int movementSpeed, vector<Ball> &ballVec, bool pulseCharged, long double &startTime){

	switch(c){
		case 'a':							//A S W D controls movement
			playerX-= movementSpeed;
			if(playerX <= playerSize){
				playerX = playerSize;
			}
			break;
		case 's':
			playerY += movementSpeed;
			if(playerY >= ht - playerSize){
				playerY = ht - playerSize;
			}
			break;
		case 'd':
			playerX += movementSpeed;
			if(playerX >= wd - playerSize){
				playerX = wd - playerSize;
			}
			break;
		case 'w':
			playerY -= movementSpeed;
			if(playerY <= playerSize){
				playerY = playerSize;
			}
			break;
		case 32:						//Spacebar for "pulse"
			if(pulseCharged){
				player_Pulse(ballVec);		//Calls pulse function
				pulseCharged==false;		//Resets flag
				startTime = time(0);		//Resets timer
			}
			break;		
		case 'q':						//Quits
			run = false;
			break;
	}
}


//Function to move vector of balls on screen
void move_Balls(vector<Ball> &ballVec, int wd, int ht, int &level, int &playerX, int &playerY, bool &pulseCharged, long double &startTime){

	int numBalls = ballVec.size();

	long double currentTime = time(0);		//Checks current time (to track pulse)

	if(currentTime-startTime>5){				//Decides if pulse is charged
		gfx_text(1360, 20, "Pulse: Ready");
		pulseCharged = true;
	}
	else{
		gfx_text(1360, 20, "Pulse: Charging...");
		pulseCharged = false;
	}

	if(numBalls==0){					//If balls are all gone, move to next level
		playerX = wd/2;
		playerY = ht/2;
		level++;
		startTime = time(0);			//Restart time
	}

	for(int i = 0; i<numBalls; i++){

		ballVec[i].setXCoor(ballVec[i].getXCoor() + ballVec[i].getXVel());
		ballVec[i].setYCoor(ballVec[i].getYCoor() + ballVec[i].getYVel());


		if(ballVec[i].getXCoor() <= ballVec[i].getRadius()){			//If ball hits left wall
			ballVec[i].setXVel(-1 * ballVec[i].getXVel());				//Make X component of vel negative
			ballVec[i].setXCoor(ballVec[i].getRadius());				//Reset ball out of wall
		}
	
		if(ballVec[i].getXCoor() >= wd - ballVec[i].getRadius()){		//If ball hits right wall
			ballVec[i].setXVel(-1 * ballVec[i].getXVel());				//Make X component of vel negative
			ballVec[i].setXCoor(wd-ballVec[i].getRadius());				//Reset ball out of wall
		}
		
		if(ballVec[i].getYCoor() >= ht - ballVec[i].getRadius()){		//If ball hits bottom wall
			ballVec[i].setYVel(-1 * ballVec[i].getYVel());				//Make Y component of vel negative
			ballVec[i].setYCoor(ht-ballVec[i].getRadius());				//Reset ball out of wall
		}

		if(ballVec[i].getYCoor() <= ballVec[i].getRadius()){			//If ball hits top wall
			ballVec[i].setYVel(-1 * ballVec[i].getYVel());				//Make Y component of vel negative
			ballVec[i].setYCoor(ballVec[i].getRadius());				//Reset ball out of wall
		}
	}
}

//Function to draw all circles of balls
void display_Balls(vector<Ball> ballVec){

	for(int i=0; i<ballVec.size(); i++){
		gfx_circle(ballVec[i].getXCoor(), ballVec[i].getYCoor(), ballVec[i].getRadius());
	}
}


//Checks if balls have collided with player, if so returns true
bool player_Collision_Check(vector<Ball> ballVec, int playerX, int playerY, int playerSize){

	for(int i = 0; i<ballVec.size(); i++){
		int xCoor = ballVec[i].getXCoor();
		int yCoor = ballVec[i].getYCoor();
		int radius = ballVec[i].getRadius();
		int sideLength = playerSize/sqrt(2);

		if(sqrt(pow(playerX-xCoor, 2)+pow(playerY-yCoor,2)) < sideLength+radius){
			return true;
		}
		
		else if(i==ballVec.size()-1){
			return false;
		}
	}
}

//Function checks to see when player collision with ball ends (otherwise player loses more than  one  life in one collision)
void collision_Confirmation(int &lifeCounter,int &lifeTracker, bool &collisionFlag,vector<Ball> ballVec, int playerX, int playerY, int playerSize){

	if(collisionFlag==false && player_Collision_Check(ballVec, playerX, playerY, playerSize)){	//If in collision
		lifeCounter--;		//Decrease player lives
		lifeTracker++;		//Increase count to track hits
		collisionFlag=true;
	}

	if(!player_Collision_Check(ballVec, playerX, playerY, playerSize))
		collisionFlag = false;
}

//Function checks to see if balls are colliding
void ball_Collision_Check(vector<Ball> &ballVec){

	int numBalls = ballVec.size();
	vector<Ball> tempVec;		//TempVec to store new balls
	bool collisionFlag=false;
	int hackCounter=0;

	for(int a=0; a<numBalls; a++){		//Fills tempVec with balls from ballVec
		tempVec.push_back(ballVec[a]);
	}

	for(int i=0; i<numBalls-1; i++){		//Nested loops check all balls for collision
		int xCoor1 = ballVec[i].getXCoor();
		int yCoor1 = ballVec[i].getYCoor();
		int radius1 = ballVec[i].getRadius();
		
		for(int j=i+1; j<numBalls; j++){
			int xCoor2 = ballVec[j].getXCoor();
			int yCoor2 = ballVec[j].getYCoor();
			int radius2 = ballVec[j].getRadius();

			//If in collision, collision bool is set to true and collision is flagged
			if(sqrt(pow(xCoor1-xCoor2,2)+pow(yCoor1-yCoor2, 2)) < radius1+radius2 && numBalls>1){	
				collisionFlag = true;
				ballVec[i].setCollisionStatus(true);
				ballVec[j].setCollisionStatus(true);
			}
		}
	}
	
	for(int k=0; k<numBalls; k++){
		
		int xCoor = ballVec[k].getXCoor();
		int yCoor = ballVec[k].getYCoor();
		int angle = ballVec[k].getStartAngle();
		double velocity = ballVec[k].getVelocity();
		int radius = ballVec[k].getRadius();
		int strength = ballVec[k].getStrength();

		if(ballVec[k].getCollisionStatus()){	//If  a collision is detected
			hackCounter++;
			if(hackCounter==1)		//Clears the tempVec to fill it with new balls
				tempVec.clear();
			if(strength>1){			//If strength>1, two smaller, faster balls are created at 45 degree angles from the original
				Ball one(xCoor+(1.8*radius)*cos(angle+45), yCoor+(1.8*radius)*sin(angle+45), angle+45, velocity*1.1, radius*0.7, false, strength-1);
				Ball two(xCoor-(2*radius)*cos(angle-45), yCoor-(2*radius)*sin(angle-45), angle-45, velocity*1.1, radius*0.7, false, strength-1);
				tempVec.push_back(one);
				tempVec.push_back(two);		//Balls are added to tempVec
			}
		}
	}

	if(collisionFlag==true){			//Fills the rest of tempVec with the balls that did not collide
		for(int m=0; m<numBalls; m++){
			if(ballVec[m].getCollisionStatus()==false){
				tempVec.push_back(ballVec[m]);
			}
		}
	}

	ballVec.clear();			//Clears ballVec
	ballVec = tempVec;			//Populates ballVec with tempVec
	tempVec.clear();
}

//Function to "pulse" (destroys every ball by one size)
void player_Pulse(vector<Ball> &ballVec){

	int numBalls = ballVec.size();
	vector<Ball> tempVec;		//Similar method as in ball_Collision_Check using tempVec

	for(int k=0; k<numBalls; k++){
		
		int xCoor = ballVec[k].getXCoor();
		int yCoor = ballVec[k].getYCoor();
		int angle = ballVec[k].getStartAngle();
		double velocity = ballVec[k].getVelocity();
		int radius = ballVec[k].getRadius();
		int strength = ballVec[k].getStrength();

		if(strength>1){		//Creates new balls and populates vector
			Ball one(xCoor+(1.8*radius)*cos(angle+45), yCoor+(1.8*radius)*sin(angle+45), angle+45, velocity*1.1, radius*0.7, false, strength-1);
			Ball two(xCoor-(2*radius)*cos(angle-45), yCoor-(2*radius)*sin(angle-45), angle-45, velocity*1.1, radius*0.7, false, strength-1);
			tempVec.push_back(one);
			tempVec.push_back(two);
		}	
	}

	ballVec.clear();
	ballVec = tempVec;
	tempVec.clear();
}


void draw_Square(int xCoor, int yCoor, int radius){			//Draws playerSquare

	for(double i=(M_PI/4); i<=(7*M_PI)/4; i+=(2*M_PI)/4)
		gfx_line(xCoor-radius*cos(i), yCoor - radius*sin(i), xCoor-radius*cos(i+(2*M_PI)/4), yCoor-radius*sin(i+(2*M_PI)/4));
}

//Function to read in balls from text file and create levels
//Yes I'm aware that this wraps around I'm sorry please be merciful
void fileEnteredBalls(string filename, vector<Ball> &ballVec1, vector<Ball> &ballVec2, vector<Ball> &ballVec3, vector<Ball> &ballVec4, vector<Ball> &ballVec5, vector<Ball> &ballVec6, vector<Ball> &ballVec7, vector<Ball> &ballVec8, vector<Ball> &ballVec9, vector<Ball> &ballVec10, bool &errorFlag){

	ifstream ifs;
	int counter = 0;
	int testInt;
	double testDouble;
	bool testBool;

	Ball testBall;
	
	ifs.open(filename);
	if(!ifs){							//Checks that file is valid
		cout<<"Error opening file"<<endl;
		errorFlag = true;
	}
	else{
		while(ifs.peek() != EOF){		//Creates a ball using info in text file
			counter++;

			ifs>>testInt;
			testBall.setXCoor(testInt);

			ifs>>testInt;
			testBall.setYCoor(testInt);

			ifs>>testInt;
			testBall.setStartAngle(testInt);

			ifs>>testDouble;
			testBall.setVelocity(testDouble);

			ifs>>testDouble;
			testBall.setRadius(testDouble);

			ifs>>testBool;
			testBall.setCollisionStatus(testBool);

			ifs>>testInt;
			testBall.setStrength(testInt);

			testBall.setXVel(testBall.getVelocity()*cos((testBall.getStartAngle()*M_PI/180)));
			testBall.setYVel(testBall.getVelocity()*sin((testBall.getStartAngle()*M_PI/180)));

			if(counter<=2){					//Populates each ballVec with a certain number of balls
				ballVec1.push_back(testBall);		//Because this is hardcoded, it becomes tedious to change the number of balls per level
			}										//I hardcoded because I wanted to actually "design" set levels

			if(counter>2 && counter<=6){
				ballVec2.push_back(testBall);
			}

			if(counter>6 && counter<=10){
				ballVec3.push_back(testBall);
			}

			if(counter>10 && counter<=15){
				ballVec4.push_back(testBall);
			}

			if(counter==16){
				ballVec5.push_back(testBall);
			}

			if(counter>16 && counter<=24){
				ballVec6.push_back(testBall);
			}

			if(counter>24 && counter<=28){
				ballVec7.push_back(testBall);
			}

			if(counter>28 && counter<=32){
				ballVec8.push_back(testBall);
			}

			if(counter>32 && counter<=35){
				ballVec9.push_back(testBall);
			}

			if(counter>35 && counter<=42){
				ballVec10.push_back(testBall);
			}
		}
	}
}


//Calls all appropriate functions to run the game
void runLevel(int &playerX, int &playerY, int &playerSize, int &lifeCounter, bool &run, int &movementSpeed, int &lifeTracker, int wd, int ht, vector<Ball> &ballVec, bool &collisionFlag,int &level, bool &pulseCharged, long double &startTime){

	player_square(playerX, playerY, playerSize, lifeCounter, run, movementSpeed,lifeTracker);		//Checks player movement
	
	if(gfx_event_waiting()){				//Waits for event
		char c=gfx_wait();
		player_Actions(playerX, playerY, wd, ht, c, run, playerSize, movementSpeed, ballVec, pulseCharged, startTime);
	}

	move_Balls(ballVec, wd, ht,level, playerX, playerY, pulseCharged, startTime);		//Adds to ball coordinates
	
	display_Balls(ballVec);			//Displays ball circles

	collision_Confirmation(lifeCounter,lifeTracker,collisionFlag, ballVec, playerX,playerY,playerSize);

	ball_Collision_Check(ballVec);

	gfx_flush();
}


//Displays a menu screen
void menuScreen(int &level, bool &playGame){

	//For ease of coding this was hardcoded but could (hypothetically) have been done exclusively using width/height info

	gfx_line(350, 500, 350+275*cos(M_PI/2.5), 500-275*sin(M_PI/2.5));		//A
	gfx_line(350+275*cos(M_PI/2.5), 500-275*sin(M_PI/2.5), 535, 500);
	gfx_line(325, 400, 325+225*cos(M_PI/7), 450-225*sin(M_PI/8));

	gfx_line(570, 420, 600, 380);		//S
	gfx_line(570, 420, 600, 460);
	gfx_line(600, 460, 570, 500);

	gfx_line(660, 500, 660, 410);		//T
	gfx_line(630, 430, 690, 390);

	gfx_line(725, 410, 775, 380);		//E
	gfx_line(725, 410, 760, 440);
	gfx_line(760, 440, 725, 470);
	gfx_line(725, 470, 774, 500);

	gfx_line(820, 380, 820, 500);		//R
	gfx_line(820, 380, 853, 423);
	gfx_line(853, 423, 820, 455);
	gfx_line(820,455, 853, 500);

	gfx_line(935, 380, 975, 440);		//O
	gfx_line(935, 380, 895, 440);
	gfx_line(975, 440, 935, 500);
	gfx_line(895, 440, 935, 500);

	gfx_line(1025, 500,1025, 380);		//I

	gfx_line(1090, 500, 1090, 380);		//D
	gfx_line(1090, 380, 1130, 440);
	gfx_line(1090, 500, 1130, 440);

	gfx_text(675, 575, "Use A S D W to control your square");		//Starting instructions

	gfx_text(675, 595, "When pulse is charged, hit Space to use it");

	gfx_text(675, 615, "Hit 'p' to start");

	char c=gfx_wait();

	if(c=='p')			//If player presses p, start the first level
		level++;
	
	else if(c=='q')			//If player press q, quit
		 playGame = false;
}

//Function to display a level loading screen before each level
//Not ultimately used
void level_Loading_Screen(int level){

	string levelString = to_string(level);

	gfx_text(735, 450, "Level");

	gfx_text(760, 450 ,levelString.c_str());

}

//Displays game over screen
void game_Over(int &level, bool &playGame, int levelsSurvived, bool &run, int &lifeCount, int &lifeTracker,int &playerSize, int &movementSpeed, int &xCoor, int &yCoor, int wd, int ht, string filename, vector<Ball> &ballVec1, vector<Ball> &ballVec2, vector<Ball> &ballVec3, vector<Ball> &ballVec4, vector<Ball> &ballVec5, vector<Ball> &ballVec6, vector<Ball> &ballVec7, vector<Ball> &ballVec8, vector<Ball> &ballVec9, vector<Ball> &ballVec10, bool errorFlag){


	ballVec1.clear(); ballVec2.clear(); ballVec3.clear(); ballVec4.clear(); ballVec5.clear(); ballVec6.clear(); ballVec7.clear(); ballVec8.clear(); ballVec9.clear(); ballVec10.clear();	//Clears all vectors to be reset

	fileEnteredBalls(filename, ballVec1,ballVec2,ballVec3,ballVec4,ballVec5,ballVec6,ballVec7,ballVec8,ballVec9,ballVec10, errorFlag);		//Loads balls into vectors

	string levelString = to_string(levelsSurvived);
	char c;

	gfx_text(722, 420, "GAME OVER");
	gfx_text(680, 465, "You made it to level ");
	gfx_text(808, 465, levelString.c_str());
	gfx_text(680, 490, "To play again press 'p'.");
	gfx_text(680, 515, "To quit press 'q'.");

	if(gfx_event_waiting()){
		c = gfx_wait();
	}

	if(c=='p'){		//Play again, resets game
		level=1;
		run = true;
		lifeCount = 3;
		lifeTracker = 0;
		playerSize = 27;
		movementSpeed = 35;
		xCoor = wd/2;
		yCoor = ht/2;
	}

	if(c=='q'){			//Quit
		playGame = false;
	}
}

//Function to display victory if player beats all levels
void victory_Screen(int &level, bool &playGame, int &lifeCount, int &lifeTracker, int &playerSize, int &movementSpeed, int &xCoor, int &yCoor, int wd, int ht, string filename, vector<Ball> &ballVec1, vector<Ball> &ballVec2, vector<Ball> &ballVec3, vector<Ball> &ballVec4, vector<Ball> &ballVec5, vector<Ball> &ballVec6, vector<Ball> &ballVec7, vector<Ball> &ballVec8, vector<Ball> &ballVec9, vector<Ball> &ballVec10, bool errorFlag){

	ballVec1.clear(); ballVec2.clear(); ballVec3.clear(); ballVec4.clear(); ballVec5.clear(); ballVec6.clear(); ballVec7.clear(); ballVec8.clear(); ballVec9.clear(); ballVec10.clear();		//Clears vectors for reset

	fileEnteredBalls(filename, ballVec1,ballVec2,ballVec3,ballVec4,ballVec5,ballVec6,ballVec7,ballVec8,ballVec9,ballVec10, errorFlag);		//Loads balls into vectors

	char c;

	gfx_text(724, 420, "VICTORY!");

	gfx_text(645, 465, "Congratulations! You beat ASTEROID");

	gfx_text(680, 490, "To play again press 'p'.");

	gfx_text(680, 515, "To quit press 'q'.");

	if(gfx_event_waiting()){
		c =  gfx_wait();
	}

	if(c =='p'){		//Play again
		level=1;		//Resets all stats
		lifeCount = 3;
		lifeTracker = 0;
		playerSize = 27;
		movementSpeed = 35;
		xCoor = wd/2;
		yCoor = ht/2;
	}

	else if(c == 'q'){		//Quit
		playGame = false;
	}
}


