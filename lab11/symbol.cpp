//Keegan MacDonell
//Fund Comp
//11.03.19
//Lab 11: Part 1

#include <cmath>
using namespace std;

#include "gfx.h"
	
void iso_triangle(int,int);			//Prototypes
void square(int, int);
void equi_triangle(int, int);
void equi_square(int, int);
void equi_pentagon(int,int);
void equi_hexagon(int,int);
void equi_heptagon(int,int);
void equi_octagon(int,int);
void equi_nonagon(int,int);

const int SIZE = 50;
const double PI = M_PI;


int main(){

	int wd = 500;
	int ht = 400;
	int xm, ym;
	bool run = true;
	int s;

	gfx_open(wd, ht, "Symbols");		//Opens blank window

	while(run){				

		s = gfx_wait();					//Waits for event
		xm = gfx_xpos();				//Gives coors of event
		ym = gfx_ypos();

		switch(s){						//Uses ASCII values to determine action
			case 1:						//mouse click
				gfx_color(32,250,235);
				square(xm,ym);
				break;
			case 51:					//If key is "3"
				equi_triangle(xm,ym);
				break;
			case 52:					//"4"
				equi_square(xm,ym);
				break;
			case 53:					//"5"
				equi_pentagon(xm,ym);
				break;
			case 54:					//"6"
				equi_hexagon(xm,ym);	
				break;
			case 55:					//"7"
				equi_heptagon(xm,ym);
				break;
			case 56:					//"8"
				equi_octagon(xm,ym);
				break;
			case 57:					//"9"
				equi_nonagon(xm,ym);
				break;
			case 27:					//"ESC"
				gfx_clear();
				break;
			case 99:					//'c'
				gfx_color(255,255,255);
				gfx_circle(xm, ym, SIZE/2);
				break;
			case 116:					//'t'
				gfx_color(62,235,56);	
				iso_triangle(xm,ym);
				break;
			case 113:					//'q'
				run = false;			//Breaks loop
				break;
		}
	}


	return 0;
}


void iso_triangle(int xm, int ym){

	gfx_line(xm, ym-SIZE/2, xm+SIZE/4, ym+SIZE/2);				//Two iso legs
	gfx_line(xm, ym-SIZE/2, xm-SIZE/4, ym+SIZE/2);
	gfx_line(xm+SIZE/4, ym+SIZE/2, xm-SIZE/4, ym+SIZE/2);		//bottom line
}

void square(int xm, int ym){

	int radius = SIZE/2*sqrt(2);

	for(double i = PI/4; i<=(7*PI)/4; i=i+(2*PI)/4){
		gfx_line(xm-radius*cos(i), ym - radius*sin(i), xm - radius*cos(i+(2*PI)/4), ym - radius*sin(i+(2*PI)/4));
	}


}

void equi_triangle(int xm, int ym){

	int radius = SIZE/2;

	gfx_color(255, 105, 237);

	for(double i=(3*PI)/6; i<=(11*PI)/6; i=i + (4*PI)/6){
		gfx_line(xm - radius*cos(i), ym - radius*sin(i), xm - radius*cos(i+(4*PI)/6), ym - radius*sin(i+(4*PI)/6));
	}
}


void equi_square(int xm, int ym){

	int radius = SIZE/2;

	gfx_color(255, 105, 237);

	for(double i = PI/4; i<=(7*PI)/4; i=i+(2*PI)/4){
		gfx_line(xm-radius*cos(i), ym - radius*sin(i), xm - radius*cos(i+(2*PI)/4), ym - radius*sin(i+(2*PI)/4));
	}
}


void equi_pentagon(int xm, int ym){

	int radius = SIZE/2;

	gfx_color(255,105,237);

	for(double i = (5*PI)/10; i<=(21*PI)/10; i=i+(2*PI)/5){
		gfx_line(xm-radius*cos(i), ym - radius*sin(i), xm - radius*cos(i+(2*PI)/5), ym - radius*sin(i+(2*PI)/5));
	}	
}


void equi_hexagon(int xm, int ym){

	int radius = SIZE/2;

	gfx_color(255,105,237);

	for(double i = 0; i<2*PI; i=i+(2*PI)/6){
		gfx_line(xm-radius*cos(i), ym - radius*sin(i), xm - radius*cos(i+(2*PI)/6), ym - radius*sin(i+(2*PI)/6));
	}
}


void equi_heptagon(int xm, int ym){
	
	int radius = SIZE/2;

	gfx_color(255,105,237);

	for(double i = 0; i<2*PI; i=i+(2*PI)/7){
		gfx_line(xm-radius*cos(i), ym - radius*sin(i), xm - radius*cos(i+(2*PI)/7), ym - radius*sin(i+(2*PI)/7));
	}
}


void equi_octagon(int xm, int ym){
	
	int radius = SIZE/2;

	gfx_color(255,105,237);

	for(double i = 0; i<2*PI; i=i+(2*PI)/8){
		gfx_line(xm-radius*cos(i), ym - radius*sin(i), xm - radius*cos(i+(2*PI)/8), ym - radius*sin(i+(2*PI)/8));
	}
}


void equi_nonagon(int xm, int ym){
	
	int radius = SIZE/2;

	gfx_color(255,105,237);

	for(double i = 0; i<2*PI; i=i+(2*PI)/9){
		gfx_line(xm-radius*cos(i), ym - radius*sin(i), xm - radius*cos(i+(2*PI)/9), ym - radius*sin(i+(2*PI)/9));
	}
}
