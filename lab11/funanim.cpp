//Keegan MacDonell
//Fund Comp
//11.03.19
//Lab 11: Part 3

#include <cstdlib>
#include <cmath>
#include <unistd.h>
#include <time.h>
#include <vector>
#include <iostream>
using namespace std;

#include "gfx.h"

void equi_triangle(double,double,int);
void equi_square(double,double,int);
void equi_pentagon(double,double,int);

const int EARTHSIZE = 10;	//Size of Earth

struct Planet{				//Struct to hold planet info
	double xInfo;
	double yInfo;
	int radius;
	double orbitRadius;
	double velocity;
	double angle;
};

int main(){
	
	int wd = 1500, ht = 900;
	bool run = true;
	int pausetime = 60000;
	char c;
	double speedCounter = 1;			//Speed modifier
	double mercVel = 10;				//Mercury's inital velocity
	double venusVel = (880/225);		//Other velocities hardcoded by time of one orbit around sun
	double earthVel = (880/354);
	double marsVel = (880/686);
	vector<Planet> planetVec;

	Planet p1 = {750, 400, EARTHSIZE/3, 50, mercVel,270};		//Initializing planets
	Planet p2 = {750, 300, EARTHSIZE, 150, venusVel,270};
	Planet p3 = {750, 200, EARTHSIZE, 250, earthVel,270};
	Planet p4 = {750, 100, EARTHSIZE/2, 350, marsVel,270};
	planetVec.push_back(p1); planetVec.push_back(p2); planetVec.push_back(p3); planetVec.push_back(p4);

	gfx_open(wd,ht,"Shapes and Stuff");

	while(run){

		gfx_clear();

		gfx_color(252,252,252);
		gfx_text(100,100, "Simple Orbit Sim");
		gfx_text(100,115, "Press 'c' to accelerate counterclockwise.");			//Instructions
		gfx_text(100,130, "Press 'd' to accelerate clockwise.");
		gfx_text(100,145, "Press 'q' to exit.");

		gfx_circle(750,450,25);	

		gfx_color(255,128,0);
		gfx_circle(planetVec[0].xInfo, planetVec[0].yInfo, planetVec[0].radius);		//Places merc

		equi_triangle(planetVec[1].xInfo, planetVec[1].yInfo, planetVec[1].radius);		//Places venus

		equi_square(planetVec[2].xInfo, planetVec[2].yInfo, planetVec[2].radius);		//Places earth

		equi_pentagon(planetVec[3].xInfo, planetVec[3].yInfo, planetVec[3].radius);		//Places mars


		gfx_flush();

		for(int j=0; j<4; j++){
			planetVec[j].angle+=planetVec[j].velocity*speedCounter;				//Increments angle info for planet
			float radAngle = (planetVec[j].angle*(M_PI/180));
			planetVec[j].xInfo = wd/2 + planetVec[j].orbitRadius*cos(radAngle);		//Finds new location
			planetVec[j].yInfo = ht/2 + planetVec[j].orbitRadius*sin(radAngle);	
		}

		usleep(pausetime);

		if(gfx_event_waiting()){			//If button pressed
			c = gfx_wait();

			switch(c){
				case 'q':				//Quit out
					run = false;
					break;
				case 'c':				//Increase speed modifier
					speedCounter+=0.2;
					break;
				case 'd':				//Decrease speed modifier
					speedCounter-=0.2;
					break;	
			}
		}

	}

	return 0;
}


void equi_triangle(double xInfo, double yInfo, int radius){		//Function to make triangle

	gfx_color(255,255,0);
	
	for(double i=0; i<2*M_PI; i+=(2*M_PI)/3){
		gfx_line(xInfo-radius*cos(i), yInfo - radius*sin(i), xInfo-radius*cos(i+(2*M_PI)/3), yInfo-radius*sin(i+(2*M_PI)/3));
	}

}

void equi_square(double xInfo,  double yInfo, int radius){		//Function to make square

	gfx_color(0,255,255);

	for(double i=0; i<2*M_PI; i+=(2*M_PI)/4){
		gfx_line(xInfo-radius*cos(i), yInfo - radius*sin(i), xInfo-radius*cos(i+(2*M_PI)/4), yInfo-radius*sin(i+(2*M_PI)/4));
	}
}

void equi_pentagon(double xInfo, double yInfo, int radius){		//Function to make pentagon

	gfx_color(255,0,0);

	for(double i=0; i<2*M_PI; i+=(2*M_PI)/5){
		gfx_line(xInfo-radius*cos(i), yInfo - radius*sin(i), xInfo-radius*cos(i+(2*M_PI)/5), yInfo-radius*sin(i+(2*M_PI)/5));
	}
}




