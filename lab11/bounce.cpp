//Keegan MacDonell
//Fund Comp
//11.03.19
//Lab 11: Part 2

#include <cstdlib>
#include <cmath>
#include <unistd.h>
#include <time.h>
using namespace std;

#include "gfx.h"

const int VELOCITY = 3;			//Global, constant variables
const double PI = M_PI;
const int RADIUS = 35;

int main(){
	
	int wd = 600, ht = 400;				//Sets screen dimensions
	double xc = 300, yc = 200;			//Sets initial ball position
	int checkX, checkY;
	bool run = true;
	int pausetime = 5000;
	int s;
	double randAngle;
	int circleX, circleY;
	double velX, velY;
	 

	srand(time(NULL));
	randAngle = (rand() % 360) * (PI/180);		//Generates random starting angle

	velX = VELOCITY * cos(randAngle);			//Determines velocity in each components
	velY = VELOCITY * sin(randAngle);

	gfx_open(wd,ht, "Bouncing Ball");			//Opens window
	gfx_color(255,255,0);						//Sets color

	while(run){
		gfx_clear();
		xc += velX;								//Moves ball coordinates based on component velocity
		yc += velY;
		gfx_circle(xc, yc, RADIUS);				// Draws circle
		gfx_flush();
		usleep(pausetime);

		if(xc <= RADIUS){			//If ball bounces against left wall
			velX = -1*velX;
			xc = RADIUS;			//Resets ball out of wall
		}
		
		if(yc <= RADIUS){			//If ball bounces against top wall
			velY = -1*velY;
			yc = RADIUS;
		}

		if(xc >= wd - RADIUS){		//If right wall
			velX = -1*velX;
			xc = wd - RADIUS;
		}

		if(yc >= ht - RADIUS){		//If bottom wall
			velY = -1*velY;
			yc = ht - RADIUS;
		}

		if(gfx_event_waiting()){	//If event
			s = gfx_wait();
			if(s==1){
				checkX = gfx_xpos();
				checkY = gfx_ypos();

				if(checkX<=RADIUS){		//If ball is inside wall sets it outside
					xc = RADIUS;
					checkX = RADIUS;
				}

				if(checkY<=RADIUS){
					yc = RADIUS;
					checkY = RADIUS;
				}

				if(checkX>=wd-RADIUS){
					xc = wd-RADIUS;
					checkX = wd-RADIUS;
				}

				if(checkY>=ht-RADIUS){
					yc = ht-RADIUS;
					checkY = ht-RADIUS;
				}

				if(checkX>=RADIUS && checkY>=RADIUS && checkX<=wd-RADIUS && checkY<=ht-RADIUS){
					xc = gfx_xpos();
					yc = gfx_ypos();
					randAngle = (rand() % 360)*(PI/180);		//New angle
					velX = VELOCITY * cos(randAngle);			//New velocities
					velY = VELOCITY * sin(randAngle);
					gfx_circle(xc, yc, RADIUS);
				}
			}
		}
			
		else if(s==113){			//'q' to quit
			break;
		}
	}	

	return 0;
}
