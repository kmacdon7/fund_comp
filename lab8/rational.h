//Keegan MacDonell
//Fundamentals of Computing
//11.10.19
//Lab 8

// rational.h
class Rational {
	friend ostream & operator<< (ostream & os, Rational &);
	friend istream & operator>> (istream & is, Rational &);
	public:
		Rational();
		Rational(int, int);
		~Rational();
		int getNumer();
		int getDenom();
		void setNumer(int);
		void setDenom(int);
		void setRational(int,int);	    void print();
		void reduce();
		Rational operator+(Rational);
		Rational operator-(Rational);
		Rational operator*(Rational);
		Rational operator/(Rational);
	private:
		int numer;
		int denom;
};

