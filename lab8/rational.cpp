//Keegan MacDonell
//Fund Comp
//11.04.19
//Lab 8

#include <iostream>
using namespace std;

#include "rational.h"

Rational::Rational() { setRational(1,1); }

Rational::Rational(int a, int b){ setRational(a,b); }

Rational::~Rational() { }

int Rational::getNumer(){ return numer; }

int Rational::getDenom(){ return denom; }

void Rational::setNumer(int a){ numer = a;}

void Rational::setDenom(int b){ denom = b;}

void Rational::setRational(int a, int b){ setNumer(a); setDenom(b); reduce(); } //Sets and simplifies rational

void Rational::print()
{	
	if(denom<0){		//Ensures negatives are formatted correctly
		numer*=-1;
		denom*=-1;
	}

	if(denom==1)		//Simplifies output if dividing by 1
		cout<<numer<<endl;
	else
		cout<< numer <<"/"<<denom<<endl;
}

void Rational::reduce(){		//Reduces rational by finding GCD with Euclidian alg

	int tempnumer = numer;
	int tempdenom = denom;
	int a;
	int b=1;

	if(numer>denom){
		while(b!=0){
			a = tempnumer/tempdenom;
			b = tempnumer - tempdenom*a;
			tempnumer = tempdenom;
			tempdenom = b;
		}
		numer = numer/tempnumer;
		denom = denom/tempnumer;
	}
	else{
		while(b!=0){
			a = tempdenom/tempnumer;
			b = tempdenom-tempnumer*a;
			tempdenom=tempnumer;
			tempnumer=b;
		}
		numer = numer/tempdenom;
		denom = denom/tempdenom;
	}
	
}


//Overloading arithmetic operations and input/output
Rational Rational::operator+(Rational b){

	Rational s;

	s.setRational( numer*b.denom + denom*b.numer, denom*b.denom );

	return s;
}

Rational Rational::operator-(Rational b){

	Rational s;

	s.setRational( numer*b.denom - denom*b.numer, denom*b.denom );

	return s;
}

Rational Rational::operator*(Rational b){

	Rational s;

	s.setRational( numer*b.numer, denom*b.denom );
	
	return s;
}

Rational Rational::operator/(Rational b){

	Rational s;

	s.setRational( numer*b.denom, denom*b.numer );

	return s;

}

istream & operator>>(istream & is, Rational & r){

	int a, b;

	cout<< "Enter the numerator and denominator of your rational: ";
	is >> a >> b;
	r.setRational(a,b);

	return is;
}

ostream & operator<<( ostream & os, Rational & r ){

	if(r.denom<0){		//Ensures negatives are formatted correctly
		r.numer*=-1;
		r.denom*=-1;
	}
	if(r.denom==1)
		os<<r.numer<<endl;
	else
		os<< r.numer <<"/"<< r.denom <<endl;

	return os;
}


