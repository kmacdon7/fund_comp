//Keegan MacDonell
//Fund Comp
//11.10.19
//Lab 8

#include <iostream>
using namespace std;
#include "rational.h"

int main(){

  	Rational a(5,6), b(3,-6), c, d, s;		//Testing to see if b simplifies to -1/2

  	cout << "*** display a and b ***\n";
  	cout<<a;
  	cout<<b;
	cin>>d;		//Read in d using overloaded operator
  	cout << "*** display c and d ***\n";
 	cout<<c;  // recall that c was instantiated with the default constructor (1/1)
	cout<<d;

  	// add a and b using overloaded operator
  	cout << "*** display a + b ***\n";
  	s = a + b;
  	cout<<s;

	//subtract b from a
	cout<< "*** display a - b ***\n";
	s = a - b;
	cout<<s;

	//multiply a and b
	cout<< "*** display a * b ***\n";
	s = a * b;
	cout<<s;

	//divide a and b
	cout<<"*** display a / b ***\n";
	s = a / b;
	cout<<s;

  return 0;
}

