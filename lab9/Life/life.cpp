#include <iostream>
#include <stdlib.h>
#include <string>
#include <unistd.h>
#include <fstream>
#include <limits>
using namespace std;

#include "lifeboard.h"

char menuDisplay(Lifeboard);
void interactiveManip(Lifeboard&, char, bool &);


int main(int argc, char*argv[]){

	Lifeboard board;
	bool run = true;
	ifstream ifs;
	char letterStore;
	int a;
	int b;


	system("clear");

	if(argc == 1){						//Runs game in case that ony executable is entered
		cout<<"Welcome to Conway's Game of Life!"<<endl;

		while(run==true){

			char userchoice = menuDisplay(board);
			interactiveManip(board, userchoice, run);
		}
	}

	else if(argc>1){		//If more than one executable is entered
	
		for(int i=1; i<argc; i++){
			ifs.open(argv[i]);
			if(!ifs){
				cout<<"Error"<<endl;
				return 1;
			}
			
			ifs >> letterStore >> a >> b;

			while(ifs){
				board.addCell(a,b);
				ifs >> letterStore >> a >> b;
			}
			ifs.close();

		}
	
			board.showBoard();		//Shows initial configuration of board
			usleep(500000);
		while(1==1){			//Runs game continuously
			board.advance();
			board.showBoard();
			usleep(500000);
		}

	}



	return 0;
}


char menuDisplay(Lifeboard board){

	board.showBoard();		//Shows full board

	char userchoice;

	cout<<endl;
	cout<<"Please pick an option."<<endl;
	cout<<"Enter 'a' to add a new cell"<<endl;
	cout<<"Enter 'r' to remove a cell"<<endl;
	cout<<"Enter 'n' to advance simulation one step"<<endl;
	cout<<"Enter 'q' to quit"<<endl;
	cout<<"Enter 'p' to run simulation continuously"<<endl;

	cout<<"Your choice: ";
	cin>>userchoice;

	while(cin.fail()){		//Ensures input is valid
		cin.clear();
		
		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		cout<<endl;
		cout<<"Try again: ";
		cin>>userchoice;
	}

	return userchoice;
}

void interactiveManip(Lifeboard &board, char userchoice, bool &run){		//Determines how to interpret input

	if(userchoice == 'a'){
		int a;
		int b;
		cout<<"Enter the coordinates of the point to add: "<<endl;
		cin >> a >> b;
		board.addCell(a,b);
	}

	else if(userchoice == 'r'){
		board.removeCell();
	}

	else if(userchoice == 'n'){
		board.advance();
	}

	else if(userchoice == 'q'){
		run = false;
	}

	else if(userchoice == 'p'){
		while(1==1){
			board.advance();
			board.showBoard();
			usleep(500000);
		}	
	}

	else{
		system("clear");
		cout<<endl;
		cout<<"Error. Try again."<<endl;
	}
}





