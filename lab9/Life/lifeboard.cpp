//Keegan MacDonell
//Fund Comp
//11.13.19
//Lab 9
//lifeboard.cpp

#include <iostream>
#include <cstdlib>
using namespace std;

#include "lifeboard.h"

Lifeboard::Lifeboard()
{
	for(int i = 0; i<SIZE; i++){
		for(int j = 0; j<SIZE; j++){
			tempboard[i][j] = DEAD;		//Initializes all cells to DEAD
			mainboard[i][j] = DEAD;
		}
	}
}

Lifeboard::~Lifeboard() { }	//Deconstructor


void Lifeboard::showBoard()
{
	for(int i = 0; i<SIZE+2; i++){
		cout<<"-";
	}
	cout<<endl;

	for(int j = 0; j<SIZE; j++){
		cout<<"|";
		for(int k = 0; k<SIZE; k++) cout<<mainboard[j][k];		//Creates frame for board and displays all cells
		cout<<"|"<<endl;
	}

	for(int m = 0; m<SIZE+2; m++){
		cout<<"-";
	}
	cout<<endl;
}

void Lifeboard::addCell(int a, int b)
{

	system("clear");

	if(a>=SIZE || b>=SIZE || a<0 || b<0)
		cout<<"Coordinates not valid"<<endl;

	else if(mainboard[a][b] == DEAD){
		mainboard[a][b] = ALIVE;
		cout<<"Cell added to ("<<a<<","<<b<<")"<<endl;		//Allows a cell to be added to the board
	}

	else if(mainboard[a][b] == ALIVE)
		cout<<"Cell already alive"<<endl;
	
	else
		cout<<"Error"<<endl;

}

void Lifeboard::removeCell()			//Removes a cell from the board
{
	int addrow;
	int addcol;

	cout<<endl;
	cout<<"Enter the coordinates of the cell you want to remove: ";
	cin>> addrow >> addcol;

	system("clear");

	if(addrow>SIZE || addcol>SIZE || addrow<0 || addcol<0)
		cout<<"Coordinates not valid"<<endl;

	else if(mainboard[addrow][addcol] == ALIVE){
		mainboard[addrow][addcol] = DEAD;
		cout<<"Cell removed from ("<<addrow<<","<<addcol<<")"<<endl;
	}

	else if(mainboard[addrow][addcol] = DEAD)
		cout<<"Cell already dead"<<endl;
	
	else
		cout<<"Error"<<endl;
}

void Lifeboard::advance()		//Advances game one iteration
{
	int counter = 0;

	system("clear");

	for(int i = 0; i<SIZE; i++){
		for(int j = 0; j<SIZE; j++){

				if(i>0 && j>0){						//Top left corner
					if(mainboard[i-1][j-1]==ALIVE)
						counter++;
				}

				if(i>0){							//Top row
					if(mainboard[i-1][j]==ALIVE)
						counter++;
				}

				if(i>0 && j<SIZE-1){				//Top right corner
					if(mainboard[i-1][j+1]==ALIVE)
						counter++;
				}

				if(j>0){							//Left column
					if(mainboard[i][j-1]==ALIVE)
						counter++;
				}

				if(j<SIZE-1){						//Right column
					if(mainboard[i][j+1]==ALIVE)
						counter++;
				}

				if(i<SIZE-1 && j>0){				//Bottom left corner
					if(mainboard[i+1][j-1]==ALIVE)
						counter++;
				}

				if(i<SIZE-1){						//Bottom row
					if(mainboard[i+1][j]==ALIVE)
						counter++;
				}

				if(i<SIZE-1 && j<SIZE-1){			//Bottom right corner
					if(mainboard[i+1][j+1]==ALIVE)
						counter++;
				}


			if(mainboard[i][j]==DEAD && counter==3)		//Applies rules to determine if cell is dead or alive
				tempboard[i][j]=ALIVE;
			else if(mainboard[i][j]==ALIVE && counter>3)
				tempboard[i][j]=DEAD;
			else if(mainboard[i][j]==ALIVE && counter<2)
				tempboard[i][j]=DEAD;
			else if(mainboard[i][j] == ALIVE && (counter==2 || counter ==3))
				tempboard[i][j]=ALIVE;

			
			counter = 0;

		} //Inside loop
	} //Outside loop

	for(int i = 0; i<SIZE; i++){
		for(int j = 0; j<SIZE; j++){
			mainboard[i][j] = tempboard[i][j];		//Transfers values from tempboard to mainboard
		}
	}


}//Function


	

	



