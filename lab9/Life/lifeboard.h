//Keegan MacDonell
//Fund Comp
//11.13.19
//Lab 9
//lifeboard.h
#include <string>
using namespace std;

const int SIZE = 40;
const char ALIVE = 'X';
const char DEAD = ' ';

class Lifeboard
{
	public:
		Lifeboard();
		~Lifeboard();
		void showBoard();
		void addCell(int, int);
		void removeCell();
		void advance();
	private:
		char tempboard[SIZE][SIZE];
		char mainboard[SIZE][SIZE];
};

