#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

int main(int argc, char*argv[]){

	int a, b, c;

	if(argc==1){
		cout<<"Enter two integers: ";
		cin>> a >> b;
		c = a+b;
		cout<<"Sum: "<<c<<endl;
	}
	else if(argc==2){
		cout<<"Not enough arguments"<<endl;
		return 1;
	}
	else if(argc==3){
		a = atoi(argv[1]);
		b = atoi(argv[2]);
		c = a+b;
		cout<<"Sum: "<<c<<endl;
	}
	else{
		cout<<"Too many arguments"<<endl;
		return 2;
	}

	return 0;
}
