//Keegan MacDonell
//Fund Comp
//11.03.19
//Lab 7:Part 1

#include <iostream>
using namespace std;

#include "circle.h"

Circle::Circle()
{
		radius = 1;
}

Circle::Circle(float r)
{
		radius = r;
}

Circle::~Circle() { }

float Circle::getRadius(){ return radius; }

void Circle::setRadius(float r){ radius = r; }

float Circle::circumference(){ return 2*radius*3.14159; }

float Circle::area() { return radius*radius*3.14159; }

void Circle::info()
{
		cout<<"  radius: "<< radius <<endl;
		cout<<"  circumference: "<< circumference() <<endl;
		cout<<"  area: "<< area() <<endl;
}



