//Keegan MacDonell
//Fund Comp
//11.03.19
//Lab 7:Part 3

class State {
		public:
			State();
			State(string,string,string,int,int,int);
			~State();
			string getAbbrev();
			string getName();
			string getCap();
			int getPop();
			int getYear();
			int getReps();
			void setAbbrev(string);
			void setName(string);
			void setCap(string);
			void setPop(int);
			void setYear(int);
			void setReps(int);
			void printAbbrev();
			void printName();
			void printCap();
			void printPop();
			void printYear();
			void printReps();
		private:
			string abbrev;
			string name;
			string cap;
			int pop;
			int year;
			int reps;
};
