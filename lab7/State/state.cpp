//Keegan MacDonell
//Fund Comp
//11.05.19
//Lab 7:Part 3

#include <iostream>
#include <string>
using namespace std;

#include "state.h"

State::State(){
		abbrev = "No abbreviation";
		name = "No name";
		cap = "No capital";
		pop = 0;
		year = 0;
		reps = 0;
}

State::State(string a, string b, string c, int d, int e, int f){
		abbrev = a;
		name = b;
		cap = c;
		pop = d;
		year = e;
		reps = f;
}

State::~State(){ }

string State::getAbbrev(){ return abbrev; }

string State::getName() { return name; }

string State::getCap() { return cap; }

int State::getPop() { return pop; }

int State::getYear() { return year; }

int State::getReps() { return reps; }

void State::setAbbrev(string a){ abbrev = a; }

void State::setName(string b){ name = b; }

void State::setCap(string c){ cap = c; }

void State::setPop(int d){ pop = d; }

void State::setYear(int e){ year = e; }

void State::setReps(int f){ reps = f; }

void State::printAbbrev(){ cout<<abbrev<<endl; }

void State::printName(){ cout<<name<<endl; }

void State::printCap(){ cout<<cap<<endl; }

void State::printPop(){ cout<<pop<<endl; }

void State::printYear(){ cout<<year<<endl;  }

void State::printReps(){ cout<<reps<<endl; }
