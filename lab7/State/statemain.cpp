//Keegan MacDonell
//11.05.19
//Lab 7: Part 3

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <limits>
#include <iomanip>
using namespace std;

#include "state.h"

void populateVec(string, vector<State>&);
int outsideMenu();
void nameDisplay(vector<State>);
void ClearScreen();
void oneStateStats(vector<State>);
void allStateStats(vector<State>);
void functionCaller(vector<State>);

int main(){

		ifstream ifs;
		string filename;
		vector<State> stateList;

		cout<<"Please enter an input file name: ";
		cin>>filename;

		ifs.open(filename);
		if(!ifs){
				cout<<"Error opening file "<<filename<<endl;
				return 1;
		}
		else{
			cout<<"Welcome to the Super Simple State Database!"<<endl;
			populateVec(filename, stateList);
		}

		functionCaller(stateList);

}



void populateVec(string filename, vector<State> &stateList){
	
		ifstream ifs;
		State currState;
		string currString;
		string currNum_s;
		int currNum;

		ifs.open(filename);

		while(ifs.peek() != EOF){
			getline(ifs,currString,',');
			currState.setAbbrev(currString);
		
			getline(ifs,currString,',');
			currState.setName(currString);	

			getline(ifs,currString,',');
			currState.setCap(currString);

			getline(ifs,currNum_s,',');
			currNum = stoi(currNum_s);
			currState.setPop(currNum);

			getline(ifs,currNum_s,',');
			currNum = stoi(currNum_s);
			currState.setYear(currNum);

			getline(ifs,currNum_s, '\n');
			currNum = stoi(currNum_s);
			currState.setReps(currNum);

			stateList.push_back(currState);
	}
}

int outsideMenu(){

		int userchoice;
	
		cout<<"Please choose an option from the list: "<<endl;
		cout<<"  1. Display the names of all states."<<endl;
		cout<<"  2. See the information for an individual state."<<endl;
		cout<<"  3. See one statistic across all states."<<endl;
		cout<<"  4. Exit."<<endl;
		cout<<"Your choice: ";
		cin>>userchoice;

		while(cin.fail() || userchoice<1 || userchoice > 4){
			cin.clear();

			cin.ignore(numeric_limits<streamsize>::max(), '\n');
	
			cout<<endl;
			cout<<"Try again: "<<endl;
			cin>>userchoice;
		}

		return userchoice;
}


void nameDisplay(vector<State> stateList){

		ClearScreen();
		for(int i=0; i<stateList.size();i++){
			cout<<setw(2)<<i+1<<". "<<stateList[i].getName()<<endl;
	}
}


void oneStateStats(vector<State> stateList){

		int userchoice;	
		int size = stateList.size();

		ClearScreen();
		nameDisplay(stateList);
	
		while(1==1){

		cout<<"Enter 0 to return to the main menu."<<endl;
		cout<<"Enter -1 to display the list of all states."<<endl;
		cout<<"Enter the number of the state whose information you want to see: ";
		cin>>userchoice;
	
		while(cin.fail() || userchoice<-1 || userchoice>size){
			cin.clear();

			cin.ignore(numeric_limits<streamsize>::max(), '\n');

			cout<<"Try again: ";
			cin>>userchoice;
		}	

		if(userchoice==0){
			ClearScreen();
			break;
		}
		if(userchoice==-1){
			cout<<endl;
			nameDisplay(stateList);
		}
		else{
			cout<<endl;
			cout<<"State abbreviation: "<<stateList[userchoice-1].getAbbrev()<<endl;
			cout<<"State capital: "<<stateList[userchoice-1].getCap()<<endl;
			cout<<"Year of admission to US: "<<stateList[userchoice-1].getYear()<<endl;			
			cout<<"Population: "<<stateList[userchoice-1].getPop()<<endl;
			cout<<"Number of Representatives: "<<stateList[userchoice-1].getReps()<<endl;
			cout<<endl;
		}

	}
}

void ClearScreen(){
		cout<<string(100,'\n');
}


void allStateStats(vector<State> stateList){

		int userchoice;
		int size = stateList.size();

		ClearScreen();
		
		while(1==1){
			cout<<"Which statistic would you like to see?"<<endl;
			cout<<"  1. State abbreviations"<<endl;
			cout<<"  2. State capitals"<<endl;
			cout<<"  3. Years of admission to US"<<endl;
			cout<<"  4. Populations"<<endl;
			cout<<"  5. Number of Representatives"<<endl;
			cout<<"  6. Return to main menu"<<endl;
			cout<<"Please enter the number of your choice: ";
			cin>>userchoice;

				
			while(cin.fail() || userchoice<1 || userchoice>size){
				cin.clear();

				cin.ignore(numeric_limits<streamsize>::max(), '\n');

				cout<<"Try again: ";
				cin>>userchoice;
			}	

			if(userchoice==1){
				ClearScreen();
				for(int i=0; i<size; i++)
					cout<<setw(2)<<i+1<<". "<<setw(14)<<stateList[i].getName()<<": "<<stateList[i].getAbbrev()<<endl;
			}
			else if(userchoice==2){
				ClearScreen();
				for(int i=0; i<size; i++)
					cout<<setw(2)<<i+1<<". "<<setw(14)<<stateList[i].getName()<<": "<<stateList[i].getCap()<<endl;
			}
			else if(userchoice==3){
				ClearScreen();
				for(int i=0; i<size; i++)
					cout<<setw(2)<<i+1<<". "<<setw(14)<<stateList[i].getName()<<": "<<stateList[i].getYear()<<endl;
			}
			else if(userchoice==4){
				ClearScreen();
				for(int i=0; i<size; i++)
					cout<<setw(2)<<i+1<<". "<<setw(14)<<stateList[i].getName()<<": "<<stateList[i].getPop()<<endl;
			}	
			else if(userchoice==5){
				ClearScreen();
				for(int i=0; i<size; i++)
					cout<<setw(2)<<i+1<<". "<<setw(14)<<stateList[i].getName()<<": "<<stateList[i].getReps()<<endl;
			}
			else if(userchoice==6){
				ClearScreen();
				break;
			}
			else{
				cout<<"Error"<<endl;
			}


		}
}


void functionCaller(vector<State> stateList){

		while(1==1){
			int userchoice = outsideMenu();

			if(userchoice==1)
				nameDisplay(stateList);

			else if(userchoice==2)
				oneStateStats(stateList);

			else if(userchoice==3)
				allStateStats(stateList);

			else if(userchoice==4){
				cout<<"Goodbye!"<<endl;
				break;
			}

			else{
				cout<<"Error."<<endl;
			}	
		}		
}
