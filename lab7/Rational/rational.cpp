//Keegan MacDonell
//Fund Comp
//11.03.19
//Lab 7:Part 2

#include <iostream>
using namespace std;

#include "rational.h"

Rational::Rational()
	{
			numer = 1;
			denom = 1;
	}

Rational::Rational(int a, int b){numer = a; denom = b;}

Rational::~Rational() { }

int Rational::getNumer(){return numer; }

int Rational::getDenom(){return denom; }

void Rational::setNumer(int a){numer = a;}

void Rational::setDenom(int b){denom = b;}

void Rational::print()
{
		cout<< numer <<"/"<<denom<<endl;
}

Rational Rational::add(Rational b){

	Rational s;

	s.setNumer( numer*b.getDenom() + denom*b.getNumer() );
	s.setDenom( denom*b.getDenom() ) ;

	return s;
}

Rational Rational::subtract(Rational b){

	Rational s;

	s.setNumer( numer*b.getDenom() - denom*b.getNumer() );
	s.setDenom( denom*b.getDenom() );

	return s;
}

Rational Rational::multiply(Rational b){

	Rational s;

	s.setNumer( numer*b.getNumer() );
	s.setDenom( denom*b.getDenom() );
	
	return s;
}

Rational Rational::divide(Rational b){

	Rational s;

	s.setNumer( numer*b.getDenom() );
	s.setDenom( denom*b.getNumer() );

	return s;

}
