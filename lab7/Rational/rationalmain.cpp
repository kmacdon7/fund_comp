//Keegan MacDonell
//Fund Comp
//11.03.19
//Lab 7: Part 2

#include <iostream>
using namespace std;
#include "rational.h"

int main()
{
  Rational a(5,6), b(3,7), c, s;

  cout << "*** display a and b ***\n";
  a.print();
  b.print();
  cout << "*** display c ***\n";
  c.print();  // recall that c was instantiated with the default constructor

  // 'mathematically' add a and b
  cout << "*** display a + b ***\n";
  s = a.add(b);
  s.print();

	//subtract b from a
	cout<< "*** display a - b ***\n";
	s = a.subtract(b);
	s.print();

	//multiply a and b
	cout<< "*** display a * b ***\n";
	s = a.multiply(b);
	s.print();

	//divide a and b
	cout<<"*** display a / b ***\n";
	s = a.divide(b);
	s.print();

  return 0;
}

