// rectangle_s.cpp
#include <iostream>
using namespace std;

struct Rectangle {
  float length;
  float width;
};

float find_perim(float, float);
float find_area(float, float);
void display(float, float);

int main()
{
  Rectangle rect;

  cout << "enter the rectangle's length and width: ";
  cin >> rect.length;
	cin >> rect.width;

  float perim = find_perim(rect.length, rect.width);
  float area = find_area(rect.length, rect.width);

  display(perim, area);

  return 0;
}

float find_perim(float a, float b)
{
  float perim = 2*a+2*b;

	return perim;
}

float find_area(float a, float b)
{
  float area = a * b;

	return area;
}

void display(float p, float a)
{
  cout << "the perimeter is: " << p << endl;
  cout << "the area is: " << a << endl;
}

