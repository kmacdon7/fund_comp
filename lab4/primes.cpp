//Keegan MacDonell
//Fundamentals of Computing
//09.26.19
//Lab 4: Part 2

#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

//Initiates functions
void populate_array(int [],int);
void sieve_loops(int [],int);
void display_loop(int [], int);


int main()
{
	
	int primeArray[1000];		//Creates arr with one thousand elements
	int size;

	size = sizeof(primeArray)/sizeof(primeArray[0]);	//Finds number of elements

	populate_array(primeArray,size); 	//Fills arr with 1's
	sieve_loops(primeArray,size);			//Filters non-primes
	display_loop(primeArray,size);		//Outputs the table of primes
	cout<<endl;

	return 0;
}


void populate_array(int a[],int size){

	for(int i = 2; i<size; i++){
		a[i]=1;
	}	
}


void sieve_loops(int a[], int size){

	for(int i=2; i<size; i++){
		for(int j=2; j<size; j++){
			if(j!=i && (j%i)==0)
				a[j] = 0;

		}
	}	
}


void display_loop(int a[], int size){

	int counter = 0;

	for(int i=2; i<size; i++){
		if(a[i]==1){
			cout<<setw(4)<<i;
			counter = counter + 1;
		}
			if(counter == 10){
				counter = 0;
				cout<<endl;
			}
	}
}
