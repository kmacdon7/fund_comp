//Keegan MacDonell
//Fundamentals of Computing
//09.28.19
//Lab 4: Part 3

#include <iostream>
#include <cmath>
#include <iomanip>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "nd_data.h"	//Loads wins[],losses[],ties[], and bgames[]
using namespace std;

void menu_display();
void record_display(int [], int [], int [],int);
void n_losses(int [], int, int);
void n_wins(int [], int, int);
void tieyears(int [], int);
void bgameyears(int [], int);
void bgame_record(int [], int);
void coaches(int);
void xtox_record(int [], int [], int [], int);
void justforfun();
void call_functions(int,int [],int [], int [], int [],int, int,int,int);


int main(){
	
	int menuchoice;
	bool run = true;
	int currentyear = 2019;			//Year where data stops
	int mostlosses = 9;					//Max losses (hardcoded but could be found with for loop)
	int mostwins = 12;					//Min losses
	int winsize = sizeof(wins)/sizeof(wins[0]);		//Size of all arrays

	while(run==true){	
		while(run==true){					//Loop displays menu, takes menuchoice, and asses if number is valid
			menu_display();
			cin>>menuchoice;
			if(menuchoice>0 && menuchoice <=10)
				break;
			else{
				cout<<menuchoice<<" is not a valid option. Please pick a number 1-10."<<endl;
				cout<<endl;
			}
		}	
		if(menuchoice==10){				//Breaks out of outer loop if menuchoice == 10;
			cout<<"Goodbye!"<<endl;	
			break;
		}
		else
			call_functions(menuchoice,wins,losses,ties,bgames,currentyear,winsize,mostwins,mostlosses);		
			cout<<endl;
			usleep(1000000);		//Delays 1 sec

	}

	return 0;

}



//Displays menu choices
void menu_display(){

	cout<<" 1: Display the record for a given year. \n";
	cout<<" 2: Display years with at least \"n\" losses. \n";
	cout<<" 3: Display years with at least \"n\" wins. \n";
	cout<<" 4: Display years with ties. \n";
	cout<<" 5: Display the years ND played in a bowl game. \n";
	cout<<" 6: Display the bowl game record for a given year. \n";
	cout<<" 7: Display the coach for a given year. \n";
	cout<<" 8: Display the cumulative record from year \"x\" to year \"y\". \n";
	cout<<" 9: Display the years that Notre Dame has been awesome. \n";
	cout<<"10: Exit \n";
	cout<<"Enter your choice: ";
}


//Displays record in any year
void record_display(int wins[], int losses[], int ties[],int currentyear){

	int year;

	cout<<"Enter the year: ";
	cin>>year;
	if(year>=1900 && year<currentyear){
		year = year-1900;
		cout<<"Wins: "<<wins[year]<<", Losses: "<<losses[year]<<", Ties: "<<ties[year]<<endl;
	}
	else
		cout<<"We have no data for "<<year<<"."<<endl;
}

//Displays years with at least n losses
void n_losses(int losses[], int size, int mostlosses){

	int counter=0;
	int n;
	cout<<"Please enter the number of losses: ";
	cin>>n;

	if(n>0 && n<=mostlosses){
		for(int i = 0; i<size; i++){
			if(losses[i]>=n){
				cout<<setw(4)<<i+1900<<" ";
				counter = counter+1;
				if(counter==10){
					counter=0;
					cout<<endl;
				}
			}
		}
		cout<<endl;
	}
	else if(n==0){
		cout<<"That's kind of a weird question. Notre Dame went undefeated in the following years: "<<endl;
			for(int i = 0; i<size; i++){
				if(losses[i]==0){
					cout<<setw(4)<<i+1900<<" ";
					counter = counter+1;
					if(counter==10){
						counter=0;
						cout<<endl;
					}
				}
			}
			cout<<endl;
	}

	else if(n>mostlosses && n<16)				//For inputs out of realistic range
		cout<<"Notre Dame has never lost that many times in a season."<<endl;
	else if(n>=16)
		cout<<"O ye of little faith."<<endl;
	else
		cout<<"You can't have negative losses!"<<endl;
}


//Displays years with at least n wins
void n_wins(int wins[], int size, int mostwins){

	int counter=0;
	int n;
	cout<<"Please enter the number of wins: ";
	cin>>n;

	if(n>0 && n<=mostwins){
		for(int i = 0; i<size; i++){
			if(wins[i]>=n){
				cout<<setw(4)<<i+1900<<" ";
				counter = counter+1;
				if(counter==10){
					counter=0;
					cout<<endl;
				}
			}
		}
		cout<<endl;
	}
	else if(n==0)
		cout<<"That's kind of a weird question. Notre Dame has won at least one game every year since 1900."<<endl;
	else if(n>mostwins && n<16)
		cout<<"Notre Dame has never won that many times in a season."<<endl;
	else if(n>16)
		cout<<"C'mon, we aren't THAT good."<<endl;
	else
		cout<<"You can't have negative wins!"<<endl;
}


//Displays all years with tie
void tieyears(int ties[], int size){

	int counter=0;
		
	for(int i=0; i<size; i++){
		if(ties[i]>0){
			cout<<setw(4)<<i+1900<<" ";
			counter = counter+1;
			if(counter==10){
				counter=0;
				cout<<endl;
			}
		}
	}
	cout<<endl;
}


//Displays years where bowl games were played
void bgameyears(int bgames[], int size){
	
	int counter=0;
		
	for(int i=0; i<size; i++){
		if(bgames[i]>0){
			cout<<setw(4)<<i+1900<<" ";
			counter = counter+1;
			if(counter==10){
				counter=0;
				cout<<endl;
			}
		}
	}
	cout<<endl;
}


//Displays bowl game record for given year
void bgame_record(int bgames[], int currentyear){
	
	int year;

	cout<<"Enter the year: ";
	cin>>year;

	if(year>=1900 && year<currentyear){
		year = year-1900;
		if(bgames[year]==0)
			cout<<"Notre Dame was not in a bowl game in "<<year+1900<<"."<<endl;
		else if(bgames[year]==1)
			cout<<"Notre Dame won her bowl game in "<<year+1900<<"."<<endl;
		else if(bgames[year]==2)
			cout<<"Notre Dame lost her bowl game in "<<year+1900<<"."<<endl;
		else
			cout<<"Error."<<endl;
	}
	else
		cout<<"We have no data for "<<year<<"."<<endl;
}


//Displays coach for given year
void coaches(int currentyear){

	int n;

	cout<<"Enter the year: ";
	cin>>n;

	if(n<1900)
		cout<<"We have no data for "<<n<<endl;
	else if(n>=1900 && n<=1901)
		cout<<"Pat O'Dea"<<endl;
	else if(n>1901 && n<=1903)
		cout<<"James Farragher"<<endl;
	else if(n==1904)
		cout<<"Louis Salmon"<<endl;
	else if(n==1905)
		cout<<"Henry J. McGlew"<<endl;
	else if(n>1905 && n<=1907)
		cout<<"Thomas Barry"<<endl;
	else if(n==1908)
		cout<<"Victor M. Place"<<endl;
	else if(n>1908 && n<=1910)
		cout<<"Frank Longman"<<endl;
	else if(n>1910 && n<=1912)
		cout<<"John L. Marks"<<endl;
	else if(n>1912 && n<=1917)
		cout<<"Jesse Harper"<<endl;
	else if(n>1917 && n<=1930)
		cout<<"Knute Rockne"<<endl;
	else if(n>1930 && n<=1933)
		cout<<"Hunk Anderson"<<endl;
	else if(n>1933 && n<=1940)
		cout<<"Elmer Layden"<<endl;
	else if(n>1940 && n<=1943)
		cout<<"Frank Leahy"<<endl;
	else if(n==1944)
		cout<<"Edward McKeever"<<endl;
	else if(n==1945)
		cout<<"Hugh Devore"<<endl;
	else if(n>1945 && n<=1953)
		cout<<"Frank Leahy"<<endl;
	else if(n>1953 && n<=1958)
		cout<<"Terry Brennan"<<endl;
	else if(n>1958 && n<=1962)
		cout<<"Joe Kuharich"<<endl;
	else if(n==1963)
		cout<<"Hugh Devore"<<endl;
	else if(n>1963 && n<=1974)
		cout<<"Ara Parseghian"<<endl;
	else if(n>1974 && n<=1980)
		cout<<"Dan Devine"<<endl;
	else if(n>1980 && n<=1985)
		cout<<"Gerry Faust"<<endl;
	else if(n>1985 && n<=1996)
		cout<<"Lou Holtz"<<endl;
	else if(n>1996 && n<=2001)
		cout<<"Bob Davie"<<endl;
	else if(n>2001 && n<=2004)
		cout<<"Tyrone Willingham (and Kent Baer)"<<endl;
	else if(n>2004 && n<=2009)
		cout<<"Charlie Weis"<<endl;
	else if(n>2009 && n<=currentyear)
		cout<<"Brian Kelly"<<endl;
	else
		cout<<"Who knows. Could be you."<<endl;

}


//Displays cumulative record from year1 to year2 or vice versa
void xtox_record(int wins[], int losses[], int ties[], int currentyear){

	int year1;
	int year2;
	int totwins=0;
	int totlosses=0;
	int totties=0;

	cout<<"Please enter the first year: ";
	cin>>year1;
	cout<<"Please enter the second year: ";
	cin>>year2;

	if(year1>=1900 && year2>=1900 && year1<currentyear && year2<currentyear){

		if(year2>=year1){

			for(int i=year1; i<=year2; i++){
				totwins = totwins+wins[i-1900];
				totlosses=totlosses+losses[i-1900];
				totties=totties+ties[i-1900];
			}

			cout<<"Wins: "<<totwins<<", Losses: "<<totlosses<<", Ties: "<<totties<<endl;
		}

		if(year1>year2){

			for(int i=year2; i<=year1; i++){
				totwins = totwins+wins[i-1900];
				totlosses=totlosses+losses[i-1900];
				totties=totties+ties[i-1900];
			}

		cout<<"Wins: "<<totwins<<", Losses: "<<totlosses<<", Ties: "<<totties<<endl;
		}
	}

	else
		cout<<"Invalid range."<<endl;
}


//Is Notre Dame awesome?
void justforfun(){

	srand(time(NULL));

	int val = rand() % 4 + 1;

	if(val==1)
		cout<<"Every year. Obviously."<<endl;
	else if(val==2)
		cout<<"C'mon. Every year."<<endl;
	else if(val==3)
		cout<<"Every. Single. One."<<endl;
	else if(val==4){
		cout<<"I mean...";
		usleep(500000);
		cout<<" pretty much all of them."<<endl;
	}
	else
		cout<<"Error."<<endl;

}


//Function to call all other functions based on menuchoice
void call_functions(int menuchoice, int wins[],int losses[], int ties[], int bgames[], int currentyear, int size, int mostwins, int mostlosses){

	if(menuchoice==1)
		record_display(wins, losses, ties, currentyear);

	else if(menuchoice==2)
		n_losses(losses,size, mostlosses);

	else if(menuchoice==3)
		n_wins(wins,size,mostwins);

	else if(menuchoice==4)
		tieyears(ties, size);
	
	else if(menuchoice==5)
		bgameyears(bgames, size);

	else if(menuchoice==6)
		bgame_record(bgames,currentyear);

	else if(menuchoice==7)
		coaches(currentyear);
	
	else if(menuchoice==8)
		xtox_record(wins,losses,ties,currentyear);

	else if(menuchoice==9)
		justforfun();
	
	else
		cout<<"Error."<<endl;

}





