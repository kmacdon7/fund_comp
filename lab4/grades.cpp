//Keegan MacDonell
//Fundamentals of Computing
//09.26.19
//Lab 4: Part 1

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

//Initialize functions
int array_size(int []);
float avg_func(int [],int);
float std_func(int[],int,float);


int main()
{

	float mean;
	int size;
	float stdev;
	float avg;

  int grades[50] =
    { 96,73,62,87,80,63,93,79,71,99,
      82,83,80,97,89,82,93,92,95,89,
      71,97,91,95,63,81,76,98,64,86,
      74,79,98,82,77,68,87,70,75,97,
      71,94,68,87,79, -1 };

	size = array_size(grades);
	mean = avg_func(grades,size);
	stdev = std_func(grades,size,mean);
	
	cout<<fixed<<setprecision(2);
	cout<<"Your data set has "<<size<<" points. The mean of your data is "<<mean<<"\% and the standard deviation is "<<stdev<<"."<<endl; //Outputs results



  return 0;
}

//Finds size of arr using sentinel value
int array_size(int array[]){

	int counter = 0;

	while(array[counter]>=0)
		counter = counter + 1;

	return counter;
}

//Finds average
float avg_func(int array[], int size){

	float mean;
	int sum=0;

	for(int i=0;i<=size-1;i++){
		sum = sum + array[i];
	}

	mean = (float)sum/(size);

	return mean;
}

//Finds std deviation
float std_func(int array[], int size,float mean){

	float stdev;
	float squaresum=0;

	for(int i =0; i<=size-1; i++){
		squaresum=pow((array[i]-mean),2)+squaresum;
	}

	stdev = sqrt(squaresum/(size));

	return stdev;

}
