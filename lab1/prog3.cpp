#include <iostream>
using namespace std;

int main()

{
//Initializes variables
float accel;
float mass;

//Takes user inputs for the formula
cout <<"This is my program to find the force on an object using the formula F = ma. \n";
cout <<"Please enter the mass of the object in kilograms:  \n";
cin >> mass;
cout <<"Please enter the acceleration of the object in m/s^2: \n";
cin >> accel;

//Uses simple formula F = ma to calculate mass
float force = accel*mass;

cout <<"The force on the object is " << force << " N. \n";

return 0;
}
