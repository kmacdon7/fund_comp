//Keegan MacDonell
//Fundamentals of Computing
//10.16.19
//Lab 5: Part 3

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <list>
#include <iterator>
using namespace std;

void menudisplay();
void sayings_display(list<string>);
void add_saying(list<string>&);
void phrase_search(list<string>);
void save_sayings(list<string>);
void generate_saying(list<string>);
void function_caller(list<string>, list<string>&, int);
void remove_saying(list<string>&);

int main(){

	list<string> sayings;		//Initialize necessary variables
	ifstream ifs;
	string n;
	string filename;
	int userchoice;
	bool run=true;

		cout<<"Please enter an input file: ";
		cin>>filename;

		ifs.open(filename);		//Loads file
		if(!ifs){
			cout<<"Error opening file "<< filename <<endl;
			return 1;
		}

	getline(ifs, n);			//Populates vector sayings with lines from input file
	while(!ifs.eof()){
			sayings.push_back(n);
			getline(ifs,n);
	}

	while(run==true){
		while(run==true){
				menudisplay();				//Function displays menu
				cout<<"Please pick an option: ";
				cin>>userchoice;			//Takes in menuchoice
				if(userchoice<1 || userchoice>7)
					cout<<"Invalid choice. Please choose a number 1-7."<<endl;
				else
					break;
		}
		if(userchoice==7){
				cout<<"Goodbye!"<<endl;		//Breaks outer loop
				break;
		}

		else
				function_caller(sayings, sayings, userchoice);		//Calls one of six functions
	}

		return 0;
}



void menudisplay(){
	cout<<endl;
	cout<<"1. Display all sayings in the database."<<endl;
	cout<<"2. Enter a new saying."<<endl;
	cout<<"3. Search for a phrase in your sayings."<<endl;
	cout<<"4. Save all sayings in a new text file."<<endl;
	cout<<"5. Generate a random saying."<<endl;
	cout<<"6. Remove a saying from the list."<<endl;
	cout<<"7. Exit."<<endl;

}


void sayings_display(list<string> sayings){	//Loops through vector to display all sayings with numbers

	for(auto it = sayings.begin(); it!=sayings.end(); it++){
		int val = distance(sayings.begin(), it);
		cout<< val+1 << ". "<< *it <<endl;
	}
}


void add_saying(list<string> &sayings){		//Uses push_back() to add a new saying to vector

		string newsaying;

		cin.ignore();

		cout<<"Please add a new saying: ";

		getline(cin, newsaying);

		sayings.push_back(newsaying);
}


void phrase_search(list<string> sayings){		//Finds a given phrase in any saying

		string phrase;

		cin.ignore();

		cout<<"Please enter the phrase you want to find: ";

		getline(cin, phrase);

		int phraseSize = phrase.size();
		int counter=0;


		for(auto i=sayings.begin(); i!=sayings.end(); i++){		//Naive string search to compare strings
			for(auto j=(*i).begin(); j!=((*i).end()-phraseSize+1); j++){
				int k = 0;
				while(k<phraseSize && *(j+k)==phrase[k])
						k=k+1;
				if(k>=phraseSize){
						int phraseVal = distance(sayings.begin(), i);
						int phraseIndex = distance((*i).begin(), j);
						cout<<"Saying "<< phraseVal+1 << " contains the desired phrase at index "<< phraseIndex <<endl;
						counter++;
				}

			}

		}
		
		if(counter==0)
			cout<<"Phrase not found."<<endl;
}


void save_sayings(list<string> sayings){		//Saves sayings in new text file

	ofstream ofs;

	string outfile;

	cout<<"Please enter the name of your output file: ";
	cin>> outfile;

	ofs.open(outfile);

	if(!ofs){
			cout<<"Error opening " <<outfile<<endl;
	}


	for(auto i=sayings.begin(); i!=sayings.end();i++)
		ofs<< *i <<endl;

}


void generate_saying(list<string> sayings){		//Randomly generates a saying from the list

	srand(time(NULL));

	int size = distance(sayings.begin(), sayings.end());

	string generated_saying;

	int num = rand() % size;

	auto iter = sayings.begin();

	advance(iter, num);

	cout<< *iter <<endl;

}

void remove_saying(list<string> &sayings){		//Removes a saying from the list

	int userchoice;
	auto iter=sayings.begin();

	for(auto it = sayings.begin(); it!=sayings.end(); it++){
		int val = distance(sayings.begin(), it);
		cout<< val+1 << ". "<< *it <<endl;
	}

	cout<<endl;

	cout<<"Please enter the number of the saying you want to remove: ";
	cin>>userchoice;

	advance(iter, userchoice-1);

	sayings.erase(iter);

}



//Calls all other functions
void function_caller(list<string> sayings, list<string> &refsayings,int userchoice){
		
		if(userchoice==1)
				sayings_display(sayings);

		else if(userchoice==2)
				add_saying(refsayings);

		else if(userchoice==3)
				phrase_search(sayings);

		else if(userchoice==4)
				save_sayings(sayings);

		else if(userchoice==5)
				generate_saying(sayings);

		else if(userchoice==6)
				remove_saying(refsayings);

		else
				cout<<"Error."<<endl;

}



