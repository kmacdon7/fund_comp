//Keegan MacDonell
//Fundamentals of Computing
//10.18.19
//Lab 6: Part 2

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <ctype.h>
#include <iomanip>
using namespace std;

void letter_count(vector<int>&, string, int&, int&);
void table_display(vector<char>, vector<int>, int, int,int);
int totalsum(vector<int>);

int main(){ 

	vector<char> letters(26);		//Initializes vectors for storing letters and letter count
	vector<int> counter(26);
	string filename;
	ifstream ifs;
	int spacecount=0;
	int other = 0;


	for(int i=0; i<26; i++){		//Populates letter vector
		letters[i] = 97+i;
	}

		cout << "Please enter the name of the input file: ";
		cin >> filename;


		ifs.open(filename);		//Opens input file
		if(!ifs){
				cout<<"Error opening file "<<filename <<endl;
				return 1;
		}


	letter_count(counter, filename, spacecount,other);
	int totallet = totalsum(counter);
	table_display(letters, counter, totallet, spacecount,other);


		return 0;
}


//Function to sort characters into groups
void letter_count(vector<int>& counter, string filename, int &spacecount, int &other){
	
		ifstream ifs;
		char n;
		

	ifs.open(filename);
	
	ifs.get(n);
	while(ifs){
			if(isupper(n))
					n=tolower(n);
			
			if(isspace(n))
					spacecount = spacecount+1;

			else if(isalpha(n))
					counter[n-97]= counter[n-97]+1;

			else
					other++;

			ifs.get(n);
	}

}

//Function to find total number of letters counted
int totalsum(vector<int> counter){

	int totalsum=0;

	for(int i=0; i<26; i++)
			totalsum = totalsum + counter[i];


	return totalsum;
}


//Function to compute and display final values of different characters
void table_display(vector<char>letter, vector<int>values, int totallet, int spacecount,int other){

	int count=0;

	int totalchar = totallet+spacecount+other;

	for(int i=0; i<26; i++){
		cout<<setw(5)<<letter[i]<<":"<<setw(6)<<values[i];
		count = count + 1;
		if(count==6){
				cout<<endl;
				count = 0;
		}
	}
	cout<<endl;
	cout<<fixed<<setprecision(1);
	cout<<"There were "<<totallet<<" letters."<<endl;
	cout<<"There were "<<totalchar<<" total characters."<<endl;
	cout<<"There were "<<spacecount<<" white space characters."<<endl;
	cout<<"Space percentage: "<<(float(spacecount)/totalchar)*100<<"%"<<endl;
}
