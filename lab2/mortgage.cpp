//Keegan MacDonell
//Fundamentals of Computing
//09.15.19
//Lab 2: Part 2

#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main(){

float princ;	//Defines major variables
float inrate;
float mopay;
bool run = 1;

//Obtains inputs from user with while loops to filter invalid inputs

while(run){
cout<<"Please enter the amount of your loan (the principal): ";
cin>>princ;
 if(princ<0)
  cout<<"Please enter a valid principal."<<endl;
 if(princ>0)
  break;
}

while(run){
cout<<"Please enter the annual interest rate (in %): ";
cin>>inrate;
 if(inrate<0 || inrate>100)
  cout<<"Please enter a valid interest rate."<<endl;
 if(inrate>0 && inrate<=100)
  break;
}

while(run){
cout<<"Please enter your desired monthly payment: ";
cin>>mopay;
 if(mopay<0)
  cout<<"Please enter a valid monthly payment."<<endl;
 if(mopay>princ){
  cout<<"You can immediately payoff the loan."<<endl;
  run = false;
 }
 if(mopay>0)
  break;
}


float moinrate = inrate/1200; //Calculates monthly interest
float mointrst; //Defines interest, month counter, and total payment counter
int mocount = 0;
float totpaid = 0;
float testinterest = princ*moinrate;

//Displays an error message if the monthly payment is too small
if(testinterest > mopay){
 run = 0;
 cout<<"The desired monthly payment is too small to ever pay off the loan.";
}


if(run){
  //Displays headers
  cout<<"Month"<<setw(12)<<"Payment"<<setw(14)<<"Interest"<<setw(18)<<"Balance"<<endl;

  //Calculates each value through the loop up until the final payment
  while(princ>=mopay && run){
   mocount = mocount+1;
   mointrst = princ*moinrate;
   princ=princ+mointrst-mopay;
   totpaid = totpaid+mopay; 

   cout<<fixed<<setprecision(2);
   cout<<setw(3)<mocount<<setw(7)<<"$"<<setw(7)<<mopay<<setw(7)<<"$"<<setw(7)<<mointrst<<setw(9)<<"$"<<setw(9)<<princ<<endl;

  }
 
  //Initiates variable for final payment
  float finalpay;
 
  //Calculates values for final iteration
  mocount = mocount+1;
  mointrst = princ*moinrate;
  finalpay=princ+mointrst;
  totpaid=totpaid+finalpay;
  princ=0;

  //Displays final iteration
  cout<<setw(3)<<mocount<<setw(7)<<"$"<<setw(7)<<finalpay<<setw(7)<<"$"<<setw(7)<<mointrst<<setw(9)<<"$"<<setw(9)<<princ<<endl;
 
 //Calculates years and month since first payment
 int yearcount = mocount/12; 
 int remmo = mocount%12;

 //Displays final output
 cout<<"You paid a total of $"<<totpaid<< " over "<<yearcount<< " years and "<<remmo<< " months.";
}

return 0;

}
