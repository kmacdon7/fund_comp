#include <iostream>
using namespace std;
#include <cmath>

int main()
{
  double a, b, c, disc;
  double x1, x2;

  cout << "Enter the coefficients (a,b,c) of a quadratic equation: ";
  cin >> a >> b >> c;

disc = pow(b,2)-4*a*c;
x1 =(-b+sqrt(pow(b,2)-4*a*c))/(2*a);
x2 =(-b-sqrt(pow(b,2)-4*a*c))/(2*a);

if(disc>0)
 cout<<"There are two solutions: "<< x1<< " and "<<x2<< endl;
else if(disc == 0)
 cout<<"There is one solution: "<<x1<< endl;
else
 cout<<"There are no real solutions. \n";

  return 0;
}
