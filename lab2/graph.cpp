//Keegan MacDonell
//Fundamentals of Computing
//09.15.09
//Lab 2: Part 3

#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main(){

//Initiates variables
const double PI = 3.14159;
float x;
float y;
float minval = 1000;
float maxval = -1000;
float minx;
float maxx;

cout<<"A plot of y = (-e^(-x/4))*2*cos(2*pi*x) from x = 0 to x = 10."<<endl;

//Displays header
cout<<setw(5)<<"X"<<setw(12)<<"Y"<<endl;

for(x = 0; x<=10.05; x=x+0.05){
 cout<<fixed<<setprecision(2);  //Sets precision of decimals
 cout<<setw(5)<<x;		//Spacing
 y=-exp(-x/4)*2*cos(2*PI*x);	//Assigns a function to y
 cout<<setw(12)<<y;		//Spacing

 if(y<minval){			//Finds min
  minval = y;
  minx = x;
 }
 if(y>maxval){			//Finds max
 maxval = y;
 maxx = x;
 }

 if(y<=-1.8)			//Ifs to graph the function
  cout<<" ##########"<<endl;	//Negative values
 else if(y<=-1.6)
  cout<<"  #########"<<endl;
 else if(y<=-1.4)
  cout<<"   ########"<<endl;
 else if(y<=-1.2)
  cout<<"    #######"<<endl;
 else if(y<=-1.0)
  cout<<"     ######"<<endl;
 else if(y<=-0.8)
  cout<<"      #####"<<endl;
 else if(y<=-0.6)
  cout<<"       ####"<<endl;
 else if(y<=-0.4)
  cout<<"        ###"<<endl;
 else if(y<=-0.2)
  cout<<"         ##"<<endl;
 else if(y<=0)
  cout<<"          #"<<endl;
 else if(y>=1.8)
  cout<<"            ##########"<<endl;		//Positive values
 else if(y>=1.6)
  cout<<"            #########"<<endl;
 else if(y>=1.4)
  cout<<"            ########"<<endl;
 else if(y>=1.2)
  cout<<"            #######"<<endl;
 else if(y>=1.0)
  cout<<"            ######"<<endl;
 else if(y>=0.8)
  cout<<"            #####"<<endl;
 else if(y>=0.6)
  cout<<"            ####"<<endl;
 else if(y>=0.4)
  cout<<"            ###"<<endl;
 else if(y>=0.2)
  cout<<"            ##"<<endl;
 else if(y>0)
  cout<<"            #"<<endl; 
}

//Displays min and max
cout<<"The maximum of "<<maxval<<" was at x = "<<maxx<<endl;
cout<<"The minimum of "<<minval<<" was at x = "<<minx<<endl;

return 0;
}
