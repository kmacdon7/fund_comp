//Keegan MacDonell
//Fundamentals of Computing
//09.10.19

#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main()
{

int val1;
int val2;

//Obtains inputs from user
cout<<"Please enter the value of the first integer: ";
cin>>val1;
cout<<"Please enter the value of the second integer: ";
cin>>val2;

//Establishes the top row using value 1
  cout<<setw(2)<<"*";
 for(int count=1; count<=val1;count++){
   cout<<setw(7)<<count;
   if(count==val1)
   cout<< endl;
 }


for(int count2=1; count2<=val2; count2++){
   cout<<setw(2)<<count2;		//Outputs the far left column
   for(int k=1; k<=val1; k++){  //Nested loops multiply each number
   cout<<setw(7)<<k*count2;
   if(k==val1)
    cout<<endl;
   }
 }

}
