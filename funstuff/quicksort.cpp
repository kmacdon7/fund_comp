#include <iostream>
#include <vector>
using namespace std;

int partition(vector<int>&, int, int);
void quicksort(vector<int>&, int, int);
void printVec(vector<int>,int);


int main()
{

	vector<int> vec = {10, 7, 8, 9, 1, 5};
	int n = vec.size();
	quicksort(vec, 0, n-1);
	cout<<"Sorted array: \n";
	printVec(vec, n);

		return 0;
}


int partition(vector<int> &vec, int low, int high){

	int index = low+(high-low)/2;
	int pivot = vec[index];
	int i = low;
	int j = high;
	int temp;

	while(i<=j){
			while(vec[i]<pivot){
					i++;
			}
			while(vec[j]>pivot){
					j--;
			}
			if(i<=j){
					temp = vec[i];
					vec[i] = vec[j];
					vec[j] = temp;
					i++;
					j--;
			}
	}
}


void quicksort(vector<int> &vec,int low, int high){

	if(low<high){
			int pivot = partition(vec,low,high);
			quicksort(vec,low,pivot-1);
			quicksort(vec,pivot,high);
	}
}


void printVec(vector<int> vec, int size){

	for(int i=0; i<size; i++)
		cout<<vec[i]<<" ";
	cout<<endl;
}
