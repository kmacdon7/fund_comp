//Keegan MacDonell
//Fundamentals of Computing
//09.11.19
//Lab 5: Part 1.1

#include <iostream>
#include <vector>
using namespace std;

void find_freqs(vector<int>, vector<int> &, int);
void results(vector<int>, int);

int main()
{
  // vector with random digits
  vector<int> digits = {4,3,6,5,7,8,9,4,6,3,1,3,5,7,6,3,6,
                  5,6,7,0,2,9,7,1,3,2,1,6,7,4,6,2,8,
                  1,4,5,6,0,2,7,6,4,5,6,8,3,5,7,1,5};

  int size = digits.size();  // find the vector's size


  // the freq vector tallies the frequency of digits
  vector<int> freq(10);   // initialize the vector to all 0's                

  find_freqs(digits, freq, size);  // compute digits' frequencies
  results(freq, 10);               // display frequencies for each digit

  return 0;
}

	//vectors are populated  and displayed using iterators
void find_freqs(vector<int> digits, vector<int> &freq, int sz)
{
		int n;
		for(int i=0; i<sz;i++){
				n=digits[i];
				freq[n]=freq[n]+1;
		}
}

void results(vector<int> freq, int s)
{
	for (int n = 0; n < s; n++) 
    cout <<  "digit " << n << " occurs " << freq[n] << " times" << endl;
}

