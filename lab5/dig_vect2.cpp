//Keegan MacDonell
//Fundamentals of Computing
//09.11.19
//Lab 5: Part 1.2

#include <iostream>
#include <vector>
#include <iterator>
#include <list>
using namespace std;

void find_freqs(vector<int>, vector<int> &, int);
void results(vector<int>, int);

int main()
{
  // vector with random digits
  vector<int> digits = {4,3,6,5,7,8,9,4,6,3,1,3,5,7,6,3,6,
                  5,6,7,0,2,9,7,1,3,2,1,6,7,4,6,2,8,
                  1,4,5,6,0,2,7,6,4,5,6,8,3,5,7,1,5};

  int size = digits.size();  // find the vector's size


  // the freq vector tallies the frequency of digits
  vector<int> freq(10);   // initialize the vector to all 0's                

  find_freqs(digits, freq, size);  // compute digits' frequencies
  results(freq, 10);               // display frequencies for each digit

  return 0;
}

//Functions use iterators to loop through, populate vectors, and display results
void find_freqs(vector<int> digits, vector<int> &freq, int sz)
{
		int n;
		for(auto itr=digits.begin(); itr<digits.end(); itr++){
				n= *itr;
				freq[n]=freq[n]+1;
		}
}

void results(vector<int> freq, int s)
{
	int val;
  for (auto itr=freq.begin(); itr < freq.end(); itr++){
		val = distance(freq.begin(), itr);
    cout <<  "digit " << val << " occurs " << *itr << " times" << endl;
	}
}

