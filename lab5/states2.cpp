// Keegan MacDonell
// Fundamentals of Computing
// 09.11.19
// Lab 5: Part 2.2

#include <iostream>
using namespace std;
#include <vector>
#include <string>
 
int main() 
{
  string line1;
  string line2 = "notre dame fighting irish";
  vector<string> states= {"Indiana", "Michigan", "Ohio", "Illinois", "Iowa"};

  cout << "enter a line: ";
  getline(cin, line1); //Gets new input line
                           

  cout << "the first line is: " << line1 << endl;
  cout << "the second line is: " << line2 << endl;

	//Loops use iterators to select and display components of vectors
  // display the states
  cout << "the states are:" << endl;
  for (auto it = states.begin(); it < states.end(); it++)
    cout << " - " << *it << endl;

  // Nested loops iterate through vector strings
 cout << "displaying the states with a space between letters:" << endl;
 for (auto it = states.begin(); it < states.end(); it++) {
   cout << " - ";
   for (auto it2 = (*it).begin(); it2 < (*it).end(); it2++)
   	cout << *it2 << " ";
   	cout << endl;
  }

  return 0;
}

