//Keegan MacDonell
//Fundamentals of Computing
//10.16.19
//Lab 5: Part 3

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
using namespace std;

void menudisplay();
void sayings_display(vector<string>);
void add_saying(vector<string>&);
void phrase_search(vector<string>);
void save_sayings(vector<string>);
void generate_saying(vector<string>);
void function_caller(vector<string>, vector<string>&, int);

int main(){

	vector<string> sayings;		//Initialize necessary variables
	ifstream ifs;
	string n;
	string filename;
	int userchoice=1;
	bool run=true;

		cout<<"Please enter an input file: ";
		cin>>filename;

		ifs.open(filename);		//Loads file
		if(!ifs){
			cout<<"Error opening file "<< filename <<endl;
			return 1;
		}

	getline(ifs, n);			//Populates vector sayings with lines from input file
	while(!ifs.eof()){
			sayings.push_back(n);
			getline(ifs,n);
	}

	while(run==true){
		while(run==true){
				menudisplay();				//Function displays menu
				cout<<"Please pick an option: ";
				cin>>userchoice;			//Takes in menuchoice
				if(userchoice<1 || userchoice>6)
					cout<<"Invalid choice. Please choose a number 1-6."<<endl;
				else
					break;
		}
		if(userchoice==6){
				cout<<"Goodbye!"<<endl;		//Breaks outer loop
				break;
		}

		else
				function_caller(sayings, sayings, userchoice);		//Calls one of five functions
	}

		return 0;
}



void menudisplay(){
	cout<<endl;
	cout<<"1. Display all sayings in the database."<<endl;
	cout<<"2. Enter a new saying."<<endl;
	cout<<"3. Search for a phrase in your sayings."<<endl;
	cout<<"4. Save all sayings in a new text file."<<endl;
	cout<<"5. Generate a random saying."<<endl;
	cout<<"6. Exit."<<endl;

}


void sayings_display(vector<string> sayings){	//Loops through vector to display all sayings

	for(int i = 0; i<sayings.size(); i++){

		cout<<"- "<<sayings[i]<<endl;
	}
}


void add_saying(vector<string> &sayings){		//Uses push_back() to add a new saying to vector

		string newsaying;

		cin.ignore();

		cout<<"Please add a new saying: ";

		getline(cin, newsaying);

		sayings.push_back(newsaying);
}


void phrase_search(vector<string> sayings){		//Finds a given phrase in any saying

		string phrase;

		cin.ignore();

		cout<<"Please enter the phrase you want to find: ";

		getline(cin, phrase);

		int phraseSize = phrase.size();
		int counter=0;


		for(int i=0; i<sayings.size(); i++){		//Naive string search to compare strings
			for(int j=0; j<sayings[i].size()-phraseSize+1; j++){
					int k=0;
					while(k<phraseSize && sayings[i][j+k]==phrase[k])
							k=k+1;
					if(k==phraseSize){
							cout<<"Phrase "<< i+1 <<" contains the desired phrase at index "<< j <<endl;
							counter = counter + 1;
					}

			}
		}
		if(counter==0)
				cout<<"Phrase not found."<<endl;
}


void save_sayings(vector<string> sayings){		//Saves sayings in new text file

	ofstream ofs;

	string outfile;

	cout<<"Please enter the name of your output file: ";
	cin>> outfile;

	ofs.open(outfile);

	if(!ofs){
			cout<<"Error opening " <<outfile<<endl;
	}


	for(int i=0; i<sayings.size();i++)
		ofs<<sayings[i]<<endl;

}


void generate_saying(vector<string> sayings){		//Randomly generates a saying from the list

	srand(time(NULL));

	int size = sayings.size();

	string generated_saying;

	int num = rand() % sayings.size();

	cout<<sayings[num]<<endl;

}


//Calls all other functions
void function_caller(vector<string> sayings, vector<string> &refsayings,int userchoice){
		
		if(userchoice==1)
				sayings_display(sayings);

		else if(userchoice==2)
				add_saying(refsayings);

		else if(userchoice==3)
				phrase_search(sayings);

		else if(userchoice==4)
				save_sayings(sayings);

		else if(userchoice==5)
				generate_saying(sayings);

		else
				cout<<"Error."<<endl;

}



