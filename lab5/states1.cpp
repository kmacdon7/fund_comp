//Keegan MacDonell
//Fundamentals of Computing
//09.11.19
//Lab 5: Part 1.1

#include <iostream>
using namespace std;
#include <vector>
#include <string>


int main() 
{
  string line1;
  string line2 = "notre dame fighting irish";
  vector<string> states= {"Indiana", "Michigan", "Ohio", "Illinois", "Iowa"};

  cout << "enter a line: ";
  getline(cin, line1); //Gets new line input
                           

  cout << "the first line is: " << line1 << endl;
  cout << "the second line is: " << line2 << endl;
	
	//Loops display results using vector indexing
  // display the states
  cout << "the states are:" << endl;
  for (int i = 0; i < states.size(); i++)
    cout << " - " << states[i] << endl;

 //Displays states with spaces
 cout << "displaying the states with a space between letters:" << endl;
 for (int i = 0; i < states.size(); i++) {
   cout << " - ";
   for (int j = 0; j < states[i].size(); j++)
   	cout << states[i][j] << " ";
   	cout << endl;
  }

  return 0;
}

