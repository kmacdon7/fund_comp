//Keegan MacDonell
//Fund Comp
//12.09.19

#include "gfx.h"
#include <cmath>
using namespace std;

void sierpinski( int x1, int y1, int x2, int y2, int x3, int y3 );
void drawTriangle( int x1, int y1, int x2, int y2, int x3, int y3);
void drawSquare(int,int,int);
void drawStar(int,int,int);
void shrinkingSquares(int,int,int);
void spiralSquares(int,int,double, double, double);
void circularLace(int,int,int);
void snowflake(int,int,int);
void tree(int,int,int,double);
void fern(int,int,double,double);
void firstSpiral(int,int,int,int,double,double);
void secondSpiral(int,int,int,int,double,double);
void thirdSpiral(int,int,int,int,double,double);

int main(){
	int width = 700, height = 700, mrgn = 20;	//Initializes board
	gfx_open(width, height, "Fractals");
	bool run = true;
	char c;

	while(run) {
		c = gfx_wait();		//Gets keyboard input

		switch(c){			//Calls functions for fractals depending on input
			case '1':	
				gfx_clear();
				sierpinski(mrgn, mrgn, width-mrgn, mrgn, width/2, height-mrgn);
				break;
			case '2':
				gfx_clear();
				shrinkingSquares(width/2, height/2, sqrt(2)*height/4);
				break;
			case '3':
				gfx_clear();
				spiralSquares(width/2, height/2, 43, 350, 0);
				break;
			case '4':
				gfx_clear();
				circularLace(width/2, height/2, 200);
				break;
			case '5':
				gfx_clear();
				snowflake(width/2, height/2, 200);
				break;
			case '6':
				gfx_clear();
				tree(width/2, height-15, 225, M_PI/2);
				break;
			case '7':
				gfx_clear();
				fern(width/2, height-15, 475, M_PI/2);
				break;
			case '8':
				gfx_clear();
				thirdSpiral(width, height/2, width/2, height/2, 300, 0);
				break;
			case 'q':
				run = false;
				break;
		}
				
  }
}

void sierpinski( int x1, int y1, int x2, int y2, int x3, int y3 )		//Triangles inside triangles
{
  // Base case. 
  if( abs(x2-x1) < 5 ) return;

  // Draw the triangle
  drawTriangle( x1, y1, x2, y2, x3, y3 );

  // Recursive calls
  sierpinski( x1, y1, (x1+x2)/2, (y1+y2)/2, (x1+x3)/2, (y1+y3)/2 );
  sierpinski( (x1+x2)/2, (y1+y2)/2, x2, y2, (x2+x3)/2, (y2+y3)/2 );
  sierpinski( (x1+x3)/2, (y1+y3)/2, (x2+x3)/2, (y2+y3)/2, x3, y3 );
}


void shrinkingSquares(int x1, int y1, int radius){					//Squares inside squares

	if(abs((x1-radius*cos(M_PI/4)) - (x1-radius*cos((3*M_PI)/4))) < 2) return;		//Base case

	drawSquare(x1, y1, radius);		//Draws square

	shrinkingSquares(x1 - radius*cos(M_PI/4), y1-radius*sin(M_PI/4), 5*radius/12);			//Calls four times for each direction
	shrinkingSquares(x1 - radius*cos(3*M_PI/4), y1-radius*sin(3*M_PI/4), 5*radius/12);
	shrinkingSquares(x1 - radius*cos(5*M_PI/4), y1-radius*sin(5*M_PI/4), 5*radius/12);
	shrinkingSquares(x1 - radius*cos(7*M_PI/4), y1-radius*sin(7*M_PI/4), 5*radius/12);
}



void spiralSquares(int x1, int y1, double radius, double spiralRadius, double angle){		//Spiral of squares

	if(spiralRadius<10) return;
	
	drawSquare(x1+spiralRadius*cos(angle), y1+spiralRadius*sin(angle), radius);		//Draws square

	spiralSquares(x1, y1, 15*radius/16, spiralRadius*=0.9, angle+=(M_PI)/4);
}


void circularLace(int x1, int y1, int radius){			//Circles on circles

	if(radius < 2) return;

	gfx_circle(x1, y1, radius);

	for(double i=0; i<2*M_PI; i+=M_PI/3){
		circularLace(x1-radius*cos(i), y1-radius*sin(i), radius/3);
	}
}

void snowflake(int x1, int y1, int radius){			//Stars on stars

	if(radius<2) return;

	drawStar(x1, y1, radius);		//Draws star

	for(double i =(3*M_PI)/2; i<(35*M_PI)/10; i+=(2*M_PI)/5){
		snowflake(x1-radius*cos(i), y1-radius*sin(i), 5*radius/13);
	}
}

void tree(int x1, int y1, int length, double angle){		//Tree with base line and lines at 30 degree angles

	if(length<2) return;

	gfx_line(x1, y1, x1-length*cos(angle), y1-length*sin(angle));

	tree(x1-length*cos(angle), y1-length*sin(angle), length/1.5, angle+M_PI/6);		//Branches
	tree(x1-length*cos(angle), y1-length*sin(angle), length/1.5, angle-M_PI/6);
}

void fern(int x1, int y1, double length, double angle){		//Fern (like tree but four times)

	if(length<2) return;

	gfx_line(x1, y1, x1-length*cos(angle), y1-length*sin(angle));

	for(double i = 1; i<=4; i++){		//Iterates four times, progressively longer
		fern(x1-length*i/4*cos(angle), y1-length*i/4*sin(angle), length/4, angle+M_PI/6);
		fern(x1-length*i/4*cos(angle), y1-length*i/4*sin(angle), length/4, angle-M_PI/6);
	}
}

void firstSpiral(int x1, int y1, int x2, int y2, double spiralRadius, double angle){		//Smallest spiral

	if(spiralRadius<1) return;

	gfx_point(x1, y1);

	firstSpiral(x1+spiralRadius*cos(angle+2*M_PI/10), y1 + spiralRadius*sin(angle+2*M_PI/10), x2, y2, 9*spiralRadius/10, angle+2*M_PI/10);
}

void secondSpiral(int x1, int y1, int x2, int y2, double spiralRadius, double angle){		//Spiral of spirals

	gfx_point(x1, y1);

	firstSpiral(x1, y1, x2, y2, spiralRadius/4, angle);

	if(spiralRadius<1) return;

	secondSpiral(x1+spiralRadius*cos(angle+2*M_PI/10), y1 + spiralRadius*sin(angle+2*M_PI/10), x2, y2, 9*spiralRadius/10, angle+2*M_PI/10);
}	


void thirdSpiral(int x1, int y1, int x2, int y2, double spiralRadius, double angle){		//Spiral of spirals of spirals

	gfx_point(x1, y1);

	secondSpiral(x1, y1, x2, y2, spiralRadius/3, angle);

	if(spiralRadius < 1) return;

	thirdSpiral(x2+spiralRadius*cos(angle+2*M_PI/10), y2 + spiralRadius*sin(angle+2*M_PI/10), x2, y2, 9*spiralRadius/10, angle+2*M_PI/10);
}



void drawTriangle( int x1, int y1, int x2, int y2, int x3, int y3 )		//Draws triangle
{
  gfx_line(x1,y1,x2,y2);
  gfx_line(x2,y2,x3,y3);
  gfx_line(x3,y3,x1,y1);
}

void drawSquare(int x1, int y1, int radius){				//Draws square

	for(double i = M_PI/4; i<=(7*M_PI)/4; i+=(2*M_PI)/4){
		gfx_line(x1 - radius*cos(i), y1 - radius*sin(i), x1-radius*cos(i+(2*M_PI)/4), y1-radius*sin(i+(2*M_PI)/4));
	}
}

void drawStar(int x1, int y1, int radius){				//Draws star

	for(double i = (3*M_PI)/2; i<(35*M_PI)/10; i+=(2*M_PI)/5){
		gfx_line(x1, y1, x1-radius*cos(i), y1-radius*sin(i));
	}
}


	



