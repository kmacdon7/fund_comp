//Author: Keegan MacDonell
//11.20.19
//Lab 10

#include <iostream>
#include <vector>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <bits/stdc++.h>
#include <cctype>
#include <fstream>
#include <iomanip>
using namespace std;

#include "cboard.h"

vector<string> anagram(vector<string>);
bool compare(string, string);
void userEnteredWords(vector<string>&);
void fileEnteredWords(vector<string>&, string);
void displayClues(Crossboard, vector<string>);
void saveBoard(Crossboard, vector<string>, string);

int main(int argc, char *argv[]){

	Crossboard board1;
	string testString;
	vector<string> testVec;
	vector<string> anagVec;
	string filename;

	if(argc==1){		//If just executable name is entered		
	
		cout<<"Anagram Crossword Puzzle Generator"<<endl;		//header
		cout<<"__________________________________"<<endl;
		cout<<endl;

		userEnteredWords(testVec);					//Gets correct input words

		sort(testVec.begin(), testVec.end(), compare);		//Sorts words in vec by length

		board1.populateBoard(testVec);

		cout<<endl;
		board1.showKey();
		cout<<endl;
		board1.showPuzzle();

		displayClues(board1, testVec);

	}

	else if(argc==2){		//Arguments are executable and text file

		filename = argv[1];
		ifstream ifs;

		ifs.open(filename);									//Checks if file is valid

		if(!ifs){
			cout<<"Error opening file " << filename <<endl;
			return 1;
		}

		else{
			
			cout<<"Anagram Crossword Puzzle Generator"<<endl;		//header
			cout<<"__________________________________"<<endl;
			
			fileEnteredWords(testVec, filename);					//Reads words from text file

			sort(testVec.begin(), testVec.end(), compare);			//Sorts by length

			board1.populateBoard(testVec);							//Fills board with words

			cout<<endl;
			board1.showKey();										//Displays both boards
			cout<<endl;
			board1.showPuzzle();

			displayClues(board1, testVec);							//Loads in and displays clues

		}
	}

	else if(argc==3){					//Saves output to new file

		string infile = argv[1];
		string outfile = argv[2];
		ifstream ifs;
		ofstream ofs;

		ifs.open(infile);

		if(!ifs){
			cout<<"Error opening file "<< infile<<endl;			//If file DNE/will not open
			return 2;
		}

		fileEnteredWords(testVec, infile);
		sort(testVec.begin(), testVec.end(), compare);

		board1.populateBoard(testVec);

		saveBoard(board1, testVec, outfile);				//Outputs boards/clues to new file
	}

	else{			//Handles case of too many arguments
		cout<<"Too many arguments. Try again."<<endl;
	}

	return 0;
}



vector<string> anagram(vector<string> passedVec){  //Function to make anagrams of words from vector loaded in

	srand(time(NULL));

	int size = passedVec.size();

	vector<string> storeVec = passedVec;

	vector<string> anagVec;

	string tempString;
	
	for(int i = 0; i<size; i++){			//Runs for number of words in vector

		int stringLen = passedVec[i].size();
		int size2 = stringLen;

		tempString = "a";					//Sets string to "a" so old words are erased

		bool run = true;

		while(run==true){							//Loop is used to determine that at least one letter is mixed
			for(int j = 0; j<size2; j++){

				int randNum = rand() % stringLen;

				if(j==0){
					tempString.push_back(passedVec[i][randNum]);		//Adds first random letter

					tempString.erase(tempString.begin());	//Deletes letter 'a' assigned above

					passedVec[i].erase(passedVec[i].begin()+randNum);  //Erases letter just used from string in vector

					stringLen--;		//Subtracts one from length of string to find new random letter
					
				}

				else{
					tempString.push_back(passedVec[i][randNum]);

					passedVec[i].erase(passedVec[i].begin()+randNum);

					stringLen--;
				}

			}

			if(tempString != storeVec[i])
				run = false;

			else{ 								//If word is not mixed, everything resets
				tempString = "a"; 
				passedVec[i] = storeVec[i]; 
				stringLen = passedVec[i].size();  
			}
		}

		anagVec.push_back(tempString);
	}
	return anagVec;
}





bool compare(string s1, string s2){		//Used to sort strings by length

	return (s1.size() > s2.size());
}






void userEnteredWords(vector<string> &stringStore){		//Controls interactive input of words

	int wordCounter = 0;
	bool parameterCheck = true;
	string testString;

	cout<<"Please enter the words for the crossword followed by a \".\""<<endl;

	while(wordCounter < 20){		//Reads in 20 words from keyboard
			
		getline(cin, testString);

		if(testString == "."){						//If '.', breaks loop
			break;
		}

		int size = testString.size();

		for(int i = 0; i<size; i++){

			if(isalpha(testString[i])){
				testString[i] = toupper(testString[i]);
			}

			else{
				parameterCheck = false;
				cout<<"Please enter only letters A-Z"<<endl;
				break;
			}
		}
		
		if(size == 1 || size>15){							//If size == 1, try again
			parameterCheck = false;
			cout<<"Please enter strings that are between 2-15 characters."<<endl;
		}

		if(parameterCheck == true){
			stringStore.push_back(testString);
			wordCounter++;
		}

		parameterCheck = true;

	}
}




void fileEnteredWords(vector<string> &stringStore, string filename){

	ifstream ifs;
	string testString;
	int failCounter = 0;
	bool parameterCheck = true;
	int wordCounter = 0;

	ifs.open(filename);

	getline(ifs, testString);					//Gets first string
	while(!ifs.eof() && wordCounter<20){

		if(testString == "."){	//If '.', breaks loop
			break;
		}

		int size = testString.size();

		for(int i = 0; i<size; i++){
			if(isalpha(testString[i])){
				testString[i] = toupper(testString[i]);				//Checks if each letter is alpha and makes upper
			}

			else{
				parameterCheck = false;
				failCounter++;
				break;
			}
		}
		
		if((size == 1 || size>15) && parameterCheck == true){			//If size == 1 or greater than 15, try again
			parameterCheck = false;
			failCounter++;
		}

		if(parameterCheck == true){				//Adds word to string vector
			stringStore.push_back(testString);
			wordCounter++;
		}

		parameterCheck = true;			//Gets next input
		getline(ifs,testString);

	}

	if(failCounter==1)
		cout<<failCounter<<" invalid entry. One entry dropped."<<endl;
	else if(failCounter>1)
		cout<<failCounter<<" invalid entries. "<<failCounter<<" entries dropped."<<endl;

}




void displayClues(Crossboard board1, vector<string> testVec){		//Function to display clues

	vector<int> rowCoorVec = board1.getRowCoors();

	vector<int> colCoorVec = board1.getColCoors();

	vector<string> orientation = board1.getOrientation();

	vector<string> anagVec = anagram(testVec);

	cout<<endl;
	cout<<"CLUES:"<<endl;
	cout<<"Location | Direction | Anagram"<<endl;

	int size = rowCoorVec.size();

	for(int i = 0; i<size; i++){
		cout<<setw(5)<<colCoorVec[i]<<","<<setw(2)<<rowCoorVec[i]<<setw(2)<<"|"<<setw(10)<<orientation[i];
		cout<<setw(2)<<"|"<<" "<<anagVec[i]<<endl;
	}
}

void saveBoard(Crossboard board1, vector<string> testVec, string ofilename){		//Function to ouput boards to text file

	ofstream ofs;
	vector<string> anagVec = anagram(testVec);
	vector<string> orientation = board1.getOrientation();
	vector<char> puzBoard = board1.getPuzBoard();
	vector<char> keyBoard = board1.getKeyBoard();
	vector<int> rowCoorVec = board1.getRowCoors();
	vector<int> colCoorVec = board1.getColCoors();
	int counter = 0;

	ofs.open(ofilename);

	if(!ofs){
		cout<<"Error opening " <<ofilename<<endl;
	}

	ofs<<"Anagram Crossword Puzzle Generator"<<endl;
	ofs<<"__________________________________"<<endl;

	ofs<<endl;
	ofs<<"Solution:"<<endl;

	for(int i = 0; i<SIZE+2; i++){				//Copies solution board over
		ofs<<"-";
	}
	ofs<<endl;

	for(int i=0; i<SIZE*SIZE; i++){
		if(counter ==0){
			ofs<<"|";
		}
		ofs<<keyBoard[i];
		counter++;
		if(counter==15){
			counter = 0;
			ofs<<"|"<<endl;
		}
	}

	for(int i = 0; i<SIZE+2; i++){
		ofs<<"-";
	}
	ofs<<endl;
	ofs<<endl;
	

	ofs<<"Crossword Puzzle:"<<endl;					//Copies puzzle board over

	for(int i = 0; i<SIZE+2; i++){
		ofs<<"-";
	}
	ofs<<endl;

	for(int i=0; i<SIZE*SIZE; i++){
		if(counter ==0){
			ofs<<"|";
		}
		ofs<<puzBoard[i];
		counter++;
		if(counter==15){
			counter = 0;
			ofs<<"|"<<endl;
		}
	}

	for(int i = 0; i<SIZE+2; i++){
		ofs<<"-";
	}
	ofs<<endl;

	ofs<<endl;
	ofs<<"CLUES:"<<endl;
	ofs<<"Location | Direction | Anagram"<<endl;				//Copies clues over

	int size = rowCoorVec.size();

	for(int i = 0; i<size; i++){
		ofs<<setw(5)<<colCoorVec[i]<<","<<setw(2)<<rowCoorVec[i]<<setw(2)<<"|"<<setw(10)<<orientation[i];
		ofs<<setw(2)<<"|"<<" "<<anagVec[i]<<endl;
	}

}

