//Keegan MacDonell
//Fundamentals of Computing
//11.19.19
//Lab 10

const int SIZE = 15;
const char KEYCHAR = '.';
const char PUZCHAR = '#';
const char BLANK = ' ';

class Crossboard
{
	public:
		Crossboard();
		~Crossboard();
		vector<string> getOrientation();
		vector<int> getRowCoors();
		vector<int> getColCoors();
		vector<char> getKeyBoard();
		vector<char> getPuzBoard();
		void showKey();
		void showPuzzle();
		void populateBoard(vector<string>&);
	private:
		char keyBoard[SIZE][SIZE];
		char puzBoard[SIZE][SIZE];
		vector<string> orientation;
		vector<int> rowCoors;
		vector<int> colCoors;
};
