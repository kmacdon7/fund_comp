//Keegan MacDonell
//Fund Comp
//11.22.19
//Lab 10

#include <iostream>
#include <string>
#include <vector>
#include <cctype>
using namespace std;

#include "cboard.h"

Crossboard::Crossboard()				//Constructor
{
	for(int i = 0; i<SIZE; i++){
		for(int j = 0; j<SIZE; j++){
			keyBoard[i][j] = KEYCHAR;
			puzBoard[i][j] = PUZCHAR;
		}
	}
}

Crossboard::~Crossboard(){ }			//Deconstructor


void Crossboard::showKey(){				//Method to display key

	cout<<"Solution:"<<endl;

	for(int i = 0; i<SIZE+2; i++){
		cout<<"-";
	}
	cout<<endl;

	for(int j = 0; j<SIZE; j++){
		cout<<"|";
		for(int k = 0; k<SIZE; k++) cout<<keyBoard[j][k];
		cout<<"|"<<endl;
	}

	for(int m = 0; m<SIZE+2; m++){
		cout<<"-";
	}
	cout<<endl;
}


void Crossboard::showPuzzle(){			//Method to display puzzle board

	for(int a = 0; a<SIZE; a++){
		for(int b = 0; b<SIZE; b++){
			if(isalpha(keyBoard[a][b]))
				puzBoard[a][b] = BLANK;			//Converts all letters on keyBoard to spaces on puzBoard
		}
	}

	cout<<"Crossword Puzzle:"<<endl;

	for(int i = 0; i<SIZE+2; i++){
		cout<<"-";
	}
	cout<<endl;

	for(int j = 0; j<SIZE; j++){
		cout<<"|";
		for(int k = 0; k<SIZE; k++) cout<<puzBoard[j][k];
		cout<<"|"<<endl;
	}

	for(int m = 0; m<SIZE+2; m++){
		cout<<"-";
	}
	cout<<endl;
}


vector<string> Crossboard::getOrientation(){		//Method to get orientations of words

	vector<string> holder;

	int size = orientation.size();

	for(int i = 0; i < size; i++){
		holder.push_back(orientation[i]);
	}

	return holder;
}

vector<int> Crossboard::getRowCoors(){			//Method to get x coordinates of words

	vector<int> holder;

	int size = rowCoors.size();

	for(int i = 0; i < size; i++){
		holder.push_back(rowCoors[i]);
	}

	return holder;
}

vector<int> Crossboard::getColCoors(){			//Method to get y coordinates of words

	vector<int> holder;

	int size = colCoors.size();

	for(int i = 0; i < size; i++){
		holder.push_back(colCoors[i]);
	}

	return holder;
}

vector<char> Crossboard::getKeyBoard(){			//Method to transfer board to vector for use in saveBoard()

	vector<char> holder;

	for(int i = 0; i < SIZE; i++){
		for(int j = 0; j < SIZE; j++){
			holder.push_back(keyBoard[i][j]);
		}
	}

	return holder;
}

vector<char> Crossboard::getPuzBoard(){			//Method to transfer board to vector for use in saveBoard()

	for(int a = 0; a<SIZE; a++){
		for(int b = 0; b<SIZE; b++){
			if(isalpha(keyBoard[a][b]))
				puzBoard[a][b] = BLANK;
		}
	}

	vector<char> holder;

	for(int i = 0; i < SIZE; i++){
		for(int j = 0; j < SIZE; j++){
			holder.push_back(puzBoard[i][j]);
		}
	}
	
	return holder;
}




void Crossboard::populateBoard(vector<string> &a){				//Method to place words on keyBoard

	int size = a.size();

	int firstWordSize = a[0].size();

	int leftoverSpace = SIZE - firstWordSize;

	int startIndex = leftoverSpace/2;

	vector<string> orientationVec;
	
	string orientationInfo;
	
	int wordsPlaced = 1;

	bool wordNotPlaced=false;


	for(int i = 0; i<firstWordSize; i++){				//Placing first word on board
		
		keyBoard[7][startIndex] = a[0][i];

		if(i==0){
			rowCoors.push_back(7);
			colCoors.push_back(startIndex);
			orientation.push_back("Across");
		}

		startIndex++;
	}


//Loops to find a matching letter

	for(int j=1; j<size; j++){					//Loops through words in vec


		if(wordNotPlaced == true){				//Breaks out of loop if no word is placed
			cout<<a[j-1]<<" could not be placed"<<endl;
			a.erase(a.begin()+j-1, a.end());
			break;
		}

		bool wordPlaced=false;
		
		bool nextWordFlag = false;

		int currentWordSize = a[j].size();

		for(int k=0; k<currentWordSize; k++){			//Loops through letters in current word

			if(nextWordFlag == true)
				break;

			wordNotPlaced=true;

			for(int m=0; m<SIZE; m++){					//Loops through rows of board

				if(wordPlaced==true){
					wordNotPlaced = false;
					wordsPlaced++;
					nextWordFlag = true;
					break;
				}

				for(int n=0; n<SIZE; n++){							//Loop through columns of board

					bool escapeFlag = false;						//Resets escape flag

					if(keyBoard[m][n] == a[j][k]){					//If letter is found on board:

						if(isalpha(keyBoard[m][n+1]) || isalpha(keyBoard[m][n-1])){		//Check for neighbors on either sides of found letter

							if(m-k>=0 && m+(currentWordSize-(k+1))<SIZE){				//Checks to make sure array bounds not exceeded by word length
								
								for(int p=k; p>0; p--){						//Checks neighbors of potential letter spots for top half of word

									if(m-k>0){								//Checks to make sure bounds are not exceeded
										if(isalpha(keyBoard[m-p-1][n])){	
											escapeFlag = true;
											break;
										}
									}

									if(isalpha(keyBoard[m-p][n]) || isalpha(keyBoard[m-p][n-1]) || isalpha(keyBoard[m-p][n+1])){
										escapeFlag = true;
										break;
									}
								}

								if(escapeFlag == false){
									for(int q=currentWordSize-k-1; q>0; q--){			//Checks neighbors for potential spots of bottom half of word

										if(m+q<SIZE-1){
											if(isalpha(keyBoard[m+q+1][n])){
												escapeFlag = true;
												break;
											}
										}

										if(isalpha(keyBoard[m+q][n]) || isalpha(keyBoard[m+q][n-1]) || isalpha(keyBoard[m+q][n+1])){
											escapeFlag = true;
											break;
										}
									}
								}

		
								if(escapeFlag == false){			//If no problem neighbors are found

									int increment=1;

									if(k==0){
										rowCoors.push_back(m);
										colCoors.push_back(n);
									}
								
									for(int r=k+1; r<currentWordSize; r++){				//Inserts bottom half of word

										keyBoard[m+increment][n] = a[j][r];

										increment++;
									}	
	
									increment = -1;

									for(int s=k-1; s>=0; s--){						//Inserts top half of word

										if(s==0){
											rowCoors.push_back(m+increment);
											colCoors.push_back(n);
										}
										
										keyBoard[m+increment][n] = a[j][s];

										increment--;
									}
									
									orientation.push_back("Down");				//Adds orientation to vector
									wordPlaced = true;
									break;

								}
							}
						}

						else if(isalpha(keyBoard[m+1][n]) || isalpha(keyBoard[m-1][n])){	//Check for neighbors above and below

							if(n-k>=0 && n+(currentWordSize-(k+1))<SIZE){				//Checks if word can fit on board

								for(int p=k; p>0; p--){							//Checks neighbors to left

									if(n-k>0){

										if(isalpha(keyBoard[m][n-p-1])){
											escapeFlag = true;
											break;
										}
									}

									if(isalpha(keyBoard[m][n-p]) || isalpha(keyBoard[m-1][n-p]) || isalpha(keyBoard[m+1][n-p])){
										escapeFlag = true;
										break;
									}
								}

								if(escapeFlag==false){
									for(int q=currentWordSize-k-1; q>0; q--){				//Checks neighbors to right

										if(n+q<SIZE-1){
											if(isalpha(keyBoard[m][n+q+1])){
												escapeFlag = true;
												break;
											}
										}

										if(isalpha(keyBoard[m][n+q]) || isalpha(keyBoard[m-1][n+q]) || isalpha(keyBoard[m+1][n+q])){
											escapeFlag = true;
											break;
										}
									}
								}

								if(escapeFlag == false){				//If no problem neighbors

									int increment=1;
										
									if(k==0){
										rowCoors.push_back(m);
										colCoors.push_back(n);
									}

									for(int r=k+1; r<currentWordSize; r++){			//Places right side of word

										keyBoard[m][n+increment] = a[j][r];
										
										increment++;
									
									}

									increment=-1;

									for(int s=k-1; s>=0; s--){						//Places left side of word

										if(s==0){
											rowCoors.push_back(m);
											colCoors.push_back(n+increment);
										}

										keyBoard[m][n+increment] = a[j][s];

										increment--;
									}
	
									wordPlaced = true;
									orientation.push_back("Across");			//Adds orientation to vector
									break;
								}
							}
						}
					}
				}
			}
		}
	}
		
	if(wordsPlaced==size-1){							//Accounts for case when word not placed was last word in array
		int size = a.size();
		cout<<a[size-1]<<" not placed"<<endl;
		a.pop_back();
	}

}






